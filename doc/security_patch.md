# Security Patches

Security patches are following the normal
[backporting](/doc/backports.md) process, but there are a few important differences.
Most importantly, the fixes **must be made in the security mirror** at
https://gitlab.com/gitlab-org/security/cluster-integration/gitlab-agent
to prevent accidental disclosure of vulnerability before we have patched
GitLab packages released.

There are some more differences:

- A Security Issue must exist and referenced in the Security Merge Requests.
- Security Patches are almost always to be patched in the current stable version
  and the previous two stable versions.
- Security Merge Requests must additionally be approved by AppSec.
  For GitLab Agent, check out the
  [Stable Counterparts in the Handbook](https://handbook.gitlab.com/handbook/engineering/development/ops/deploy/environments/#stable-counterparts).
- Approved and Ready-to-merge Security Merge Requests must be assigned to the
  GitLab Release Tools Bot (`gitlab-release-tools-bot`).
- The Security Issue must be labeled with `~security-target` when the
  Security Merge Requests are ready to be considered for the next patch release.
- The release managers / tooling will take it from here.
