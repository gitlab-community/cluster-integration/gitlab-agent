#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

# NOTE: this script is used to vendor negotiation package from k8s.io/apiserver
# into the gitlab-org/cluster-integration/gitlab-agent repository.

git_url="https://github.com/kubernetes/apiserver.git"

# SSOT of the version of the package
version="v0.31.3"

vendored_dir="$(dirname "$0")/../internal/vendored/k8s.io/apiserver/pkg/endpoints/handlers/negotiation"
mkdir -p "$vendored_dir"

# Create a temporary directory
temp_dir=$(mktemp -d)

# Clean up temporary directory on exit
trap 'rm -rf "$temp_dir"' EXIT

echo "Vendoring k8s.io/apiserver package from $git_url to temporary directory $temp_dir..."
git clone --depth 1 --branch "$version" "$git_url" "$temp_dir"

echo "Copying k8s.io/apiserver package to $vendored_dir..."
rm -rf "$vendored_dir"
cp -r "$temp_dir/pkg/endpoints/handlers/negotiation" "$vendored_dir"

make gazelle
make gazelle update-repos

echo "Vendoring k8s.io/apiserver package is successful"
