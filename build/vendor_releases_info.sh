#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

# NOTE: this script is used to vendor GitLab release information
# into the gitlab-org/cluster-integration/gitlab-agent repository.
# This information will be embedded into KAS to warn about
# KAS <-> agentk incompatibilities due to outdated versions.

releases_yaml_url='https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/master/data/releases.yml'

vendored_dir="$(dirname "$0")/../cmd/kas/kasapp/vendored"
mkdir -p "$vendored_dir"
vendored_releases_info_path="$vendored_dir/releases_info.txt"

curl --silent "$releases_yaml_url" | yq '[.[].version] | join(",")' | tr -d '[:space:]' > "$vendored_releases_info_path"
