package api

import (
	"testing"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/testhelpers"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/pkg/agentcfg"
)

func TestValidation_Valid(t *testing.T) {
	tests := []testhelpers.ValidTestcase{
		{
			Name:  "empty Configuration",
			Valid: &Configuration{},
		},
		{
			Name: "minimal1 Configuration",
			Valid: &Configuration{
				DefaultNamespace: "def",
			},
		},
		{
			Name: "minimal2 Configuration",
			Valid: &Configuration{
				DefaultNamespace: "def",
				AccessAs: &agentcfg.CiAccessAsCF{
					As: &agentcfg.CiAccessAsCF_Agent{
						Agent: &agentcfg.CiAccessAsAgentCF{},
					},
				},
			},
		},
		{
			Name: "minimal AllowedAgent",
			Valid: &AllowedAgent{
				ConfigProject: &ConfigProject{},
			},
		},
		{
			Name:  "minimal ConfigProject",
			Valid: &ConfigProject{},
		},
		{
			Name:  "minimal Pipeline",
			Valid: &Pipeline{},
		},
		{
			Name:  "minimal Project",
			Valid: &Project{},
		},
		{
			Name: "Project with groups",
			Valid: &Project{
				Groups: []*Group{
					{},
				},
			},
		},
		{
			Name:  "minimal Group",
			Valid: &Group{},
		},
		{
			Name:  "minimal Job",
			Valid: &Job{},
		},
		{
			Name: "minimal User",
			Valid: &User{
				Username: "user",
			},
		},
		{
			Name: "minimal Environment",
			Valid: &Environment{
				Slug: "prod",
				Tier: "production",
			},
		},
		{
			Name: "minimal AllowedAgentsForJob",
			Valid: &AllowedAgentsForJob{
				Job:      &Job{},
				Pipeline: &Pipeline{},
				Project:  &Project{},
				User: &User{
					Username: "user",
				},
			},
		},
	}
	testhelpers.AssertValid(t, tests)
}

func TestValidation_Invalid(t *testing.T) {
	tests := []testhelpers.InvalidTestcase{
		{
			ErrString: "validation error:\n - config_project: value is required [required]",
			Invalid:   &AllowedAgent{},
		},
		{
			ErrString: "validation error:\n - username: value length must be at least 1 bytes [string.min_bytes]",
			Invalid:   &User{},
		},
		{
			ErrString: "validation error:\n - slug: value length must be at least 1 bytes [string.min_bytes]\n - tier: value length must be at least 1 bytes [string.min_bytes]",
			Invalid:   &Environment{},
		},
		{
			ErrString: "validation error:\n - job: value is required [required]\n - pipeline: value is required [required]\n - project: value is required [required]\n - user: value is required [required]",
			Invalid:   &AllowedAgentsForJob{},
		},
	}
	testhelpers.AssertInvalid(t, tests)
}
