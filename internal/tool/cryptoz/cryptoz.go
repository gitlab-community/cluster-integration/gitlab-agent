package cryptoz

import "crypto/sha256"

const (
	// HMACSHA256MinKeySize is the minimum HMAC SHA256 key size that is considered secure.
	// See https://www.rfc-editor.org/rfc/rfc2104#section-3 and https://en.wikipedia.org/wiki/HMAC.
	HMACSHA256MinKeySize = sha256.Size
	// HMACSHA256MaxKeySize is the maximum HMAC SHA256 key size that is not hashed into 32 bytes.
	HMACSHA256MaxKeySize = sha256.BlockSize
	// HMACSHA3_512MinKeySize is the minimum HMAC SHA3 512 key size that is considered secure.
	// See https://www.rfc-editor.org/rfc/rfc2104#section-3 and https://en.wikipedia.org/wiki/HMAC.
	HMACSHA3_512MinKeySize = 64
	// HMACSHA3_512MaxKeySize is the maximum HMAC SHA3 512 key size that is not hashed into 64 bytes.
	HMACSHA3_512MaxKeySize = 72
	// AES256KeyLength is the expected key length for the Workflow data encryption.
	// crypto/aes forces a 32 key int AES-265.
	AES256KeyLength = 32
)
