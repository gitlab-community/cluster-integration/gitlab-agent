package prototool

import (
	"net/http"
	"net/url"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/httpz"
)

func (x *HttpRequest) HTTPHeader() http.Header {
	return HeaderKVToHTTPHeader(x.Header)
}

func (x *HttpRequest) URLQuery() url.Values {
	return QueryKVToURLValues(x.Query)
}

func (x *HttpRequest) IsUpgrade() bool {
	for _, kv := range x.Header {
		// According to bytes.Equal(), this doesn't allocate.
		if string(kv.Key) == httpz.UpgradeHeader {
			return true
		}
	}
	return false
}

func (x *HttpResponse) HTTPHeader() http.Header {
	return HeaderKVToHTTPHeader(x.Header)
}
