package mock_tunnel_rpc

import (
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tunnel/rpc"
	"google.golang.org/grpc"
)

//go:generate mockgen.sh -source "../../../tunnel/rpc/rpc_grpc.pb.go" -destination "rpc.go" -package "mock_tunnel_rpc" "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tunnel/rpc" "ReverseTunnel_ConnectServer,ReverseTunnel_ConnectClient,ReverseTunnelClient"

//go:generate mockgen.sh -source "doc.go" -destination "mock.go" -package "mock_tunnel_rpc"

type ReverseTunnel_ConnectServer interface {
	grpc.BidiStreamingServer[rpc.ConnectRequest, rpc.ConnectResponse]
}

type ReverseTunnel_ConnectClient interface {
	grpc.BidiStreamingClient[rpc.ConnectRequest, rpc.ConnectResponse]
}
