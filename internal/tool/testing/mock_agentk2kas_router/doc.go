package mock_agentk2kas_router

//go:generate mockgen.sh -source "../../../module/agentk2kas_tunnel/router/registry.go" -destination "registry.go" -package "mock_agentk2kas_router"

//go:generate mockgen.sh -source "../../../module/agentk2kas_tunnel/router/tracker.go" -destination "tracker.go" -package "mock_agentk2kas_router"
