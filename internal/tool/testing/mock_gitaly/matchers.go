package mock_gitaly

import (
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/gitaly/vendored/gitalypb"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_configuration"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/managed_resources"
	"go.uber.org/mock/gomock"
)

func LastCommitForPathRequestForAgentConfig(agentName string) gomock.Matcher {
	expectedPath := agent_configuration.FileForAgent(agentName)
	return gomock.Cond(func(r *gitalypb.LastCommitForPathRequest) bool {
		return string(r.Path) == expectedPath
	})
}

func TreeEntryRequestForAgentConfig(agentName string) gomock.Matcher {
	return TreeEntryRequestForAFile(agent_configuration.FileForAgent(agentName))
}

func GetTreeEntriesRequestForAllAgents() gomock.Matcher {
	return gomock.Cond(func(r *gitalypb.GetTreeEntriesRequest) bool {
		return string(r.Path) == agent_configuration.Directory
	})
}

func GetTreeEntriesRequestForAnAgent(agentName string) gomock.Matcher {
	path := managed_resources.EnvironmentTemplatesDirectoryForAgent(agentName)
	return gomock.Cond(func(r *gitalypb.GetTreeEntriesRequest) bool {
		return string(r.Path) == path
	})
}

func TreeEntryRequestForAFile(fileName string) gomock.Matcher {
	return gomock.Cond(func(r *gitalypb.TreeEntryRequest) bool {
		return string(r.Path) == fileName
	})
}

func TreeEntryRequestForATemplate(agentName, templateName string) gomock.Matcher {
	return TreeEntryRequestForAFile(managed_resources.EnvironmentTemplateFile(agentName, templateName))
}
