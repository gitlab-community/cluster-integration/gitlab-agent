// Package mock_rpc contains mocks for gRPC interfaces.
package mock_rpc

import (
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_configuration/rpc"
	"google.golang.org/grpc"
)

//go:generate mockgen.sh -destination "grpc.go" -package "mock_rpc" "google.golang.org/grpc" "ServerStream,ClientStream,ClientConnInterface,ServerTransportStream"

//go:generate mockgen.sh -source "../../../module/agent_configuration/rpc/rpc_grpc.pb.go" -destination "agent_configuration.go" -package "mock_rpc"

//go:generate mockgen.sh -source "../../../module/agent_configuration/rpc/configuration_watcher.go" -destination "configuration_watcher.go" -package "mock_rpc"

//go:generate mockgen.sh -destination "gitlab_access.go" -package "mock_rpc" "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/gitlab_access/rpc" "GitlabAccess_MakeRequestServer"

//go:generate mockgen.sh -destination "grpctool.go" -package "mock_rpc" "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/grpctool" "InboundGRPCToOutboundHTTPStream,PoolConn,PoolInterface,ServerErrorReporter"

//go:generate mockgen.sh -source "doc.go" -destination "mock.go" -package "mock_rpc"

// Workaround for https://github.com/uber-go/mock/issues/197.
type AgentConfiguration_GetConfigurationServer interface {
	grpc.ServerStreamingServer[rpc.ConfigurationResponse]
}

// Workaround for https://github.com/uber-go/mock/issues/197.
type AgentConfiguration_GetConfigurationClient interface {
	grpc.ServerStreamingClient[rpc.ConfigurationResponse]
}
