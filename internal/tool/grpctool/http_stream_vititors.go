package grpctool

import (
	"google.golang.org/protobuf/reflect/protoreflect"
)

const (
	HTTPRequestHeaderFieldNumber      protoreflect.FieldNumber = 1
	HTTPRequestDataFieldNumber        protoreflect.FieldNumber = 2
	HTTPRequestTrailerFieldNumber     protoreflect.FieldNumber = 3
	HTTPRequestUpgradeDataFieldNumber protoreflect.FieldNumber = 4

	HTTPResponseHeaderFieldNumber      protoreflect.FieldNumber = 1
	HTTPResponseDataFieldNumber        protoreflect.FieldNumber = 2
	HTTPResponseTrailerFieldNumber     protoreflect.FieldNumber = 3
	HTTPResponseUpgradeDataFieldNumber protoreflect.FieldNumber = 4
)

var (
	HTTPRequestStreamVisitor  = NewLazyStreamVisitor(&HttpRequest{})
	HTTPResponseStreamVisitor = NewLazyStreamVisitor(&HttpResponse{})
)
