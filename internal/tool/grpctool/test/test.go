package test

import (
	"context"

	"google.golang.org/grpc"
)

var (
	_ TestingServer = &GRPCTestingServer{}
)

type GRPCTestingServer struct {
	UnsafeTestingServer
	UnaryFunc     func(context.Context, *Request) (*Response, error)
	StreamingFunc func(grpc.BidiStreamingServer[Request, Response]) error
}

func (s *GRPCTestingServer) RequestResponse(ctx context.Context, request *Request) (*Response, error) {
	return s.UnaryFunc(ctx, request)
}

func (s *GRPCTestingServer) StreamingRequestResponse(server grpc.BidiStreamingServer[Request, Response]) error {
	return s.StreamingFunc(server)
}
