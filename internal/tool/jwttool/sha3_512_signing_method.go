package jwttool

import (
	"crypto"

	"github.com/golang-jwt/jwt/v5"
	_ "golang.org/x/crypto/sha3"
)

var (
	SigningMethodHS3_512 *jwt.SigningMethodHMAC
)

func init() {
	// SHA3 512
	SigningMethodHS3_512 = &jwt.SigningMethodHMAC{
		Name: "HS3-512",
		Hash: crypto.SHA3_512,
	}
	jwt.RegisterSigningMethod(SigningMethodHS3_512.Alg(), func() jwt.SigningMethod {
		return SigningMethodHS3_512
	})
}
