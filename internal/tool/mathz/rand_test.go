package mathz

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestDurationWithPositiveJitter_Negative(t *testing.T) {
	tests := []struct {
		name          string
		d             time.Duration
		jitterPercent int64
		expected      time.Duration
	}{
		{
			name:          "negative duration",
			d:             -1,
			jitterPercent: 5,
			expected:      0,
		},
		{
			name:          "zero duration",
			d:             0,
			jitterPercent: 5,
			expected:      0,
		},
		{
			name:          "no jitter",
			d:             time.Second,
			jitterPercent: 0,
			expected:      time.Second,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			j := DurationWithPositiveJitter(tt.d, tt.jitterPercent)
			assert.Equal(t, tt.expected, j)
		})
	}
}
