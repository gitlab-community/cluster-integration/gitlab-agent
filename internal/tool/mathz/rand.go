package mathz

import (
	"math/rand/v2"
	"time"
)

// DurationWithPositiveJitter returns d with an added jitter in the range [0,jitterPercent% of the value) i.e. it's additive.
func DurationWithPositiveJitter(d time.Duration, jitterPercent int64) time.Duration {
	if d <= 0 {
		return 0
	}
	if jitterPercent == 0 { // negative jitter will panic, that's ok. Don't do that.
		return d
	}
	r := (int64(d) * jitterPercent) / 100
	jitter := rand.Int64N(r) //nolint: gosec
	return d + time.Duration(jitter)
}
