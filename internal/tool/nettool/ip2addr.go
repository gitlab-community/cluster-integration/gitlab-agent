package nettool

import (
	"net"
	"net/netip"
)

func IP2Addr(ip net.IP) (netip.Addr, bool /* valid */) {
	if p4 := ip.To4(); len(p4) == net.IPv4len {
		return netip.AddrFrom4([4]byte(p4)), true
	}
	if len(ip) == net.IPv6len {
		return netip.AddrFrom16([16]byte(ip)), true
	}
	return netip.Addr{}, false // invalid
}
