package agent

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/mock_k8s"
	"go.uber.org/mock/gomock"
	kubeerrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

func TestModule_FailedToGetCRD(t *testing.T) {
	// GIVEN
	ctrl := gomock.NewController(t)
	mockDynamicClient := mock_k8s.NewMockInterface(ctrl)
	mockNamespaceableResourceInterface := mock_k8s.NewMockNamespaceableResourceInterface(ctrl)

	// setup mock expectations
	mockDynamicClient.EXPECT().Resource(gomock.Any()).Return(mockNamespaceableResourceInterface)
	mockNamespaceableResourceInterface.EXPECT().Get(gomock.Any(), testSupportedCRDResourceName, gomock.Any()).Return(nil, errors.New("expected error during test"))

	// WHEN
	ok, err := checkCRDExistsAndEstablished(context.Background(), mockDynamicClient, testSupportedCRD)

	// THEN
	assert.ErrorContains(t, err, "unable to get CRD")
	assert.False(t, ok)
}

func TestModule_CRDNotFound(t *testing.T) {
	// GIVEN
	ctrl := gomock.NewController(t)
	mockDynamicClient := mock_k8s.NewMockInterface(ctrl)
	mockNamespaceableResourceInterface := mock_k8s.NewMockNamespaceableResourceInterface(ctrl)

	// setup mock expectations
	mockDynamicClient.EXPECT().Resource(gomock.Any()).Return(mockNamespaceableResourceInterface)
	mockNamespaceableResourceInterface.EXPECT().Get(gomock.Any(), testSupportedCRDResourceName, gomock.Any()).Return(nil, kubeerrors.NewNotFound(testSupportedCRD.GroupResource(), testSupportedCRD.GroupResource().String()))

	// WHEN
	ok, err := checkCRDExistsAndEstablished(context.Background(), mockDynamicClient, testSupportedCRD)

	// THEN
	require.NoError(t, err)
	assert.False(t, ok)
}

func TestModule_CRDMultipleVersionsNoSupport(t *testing.T) {
	// GIVEN
	ctrl := gomock.NewController(t)
	mockDynamicClient := mock_k8s.NewMockInterface(ctrl)
	mockNamespaceableResourceInterface := mock_k8s.NewMockNamespaceableResourceInterface(ctrl)

	// setup mock expectations
	mockDynamicClient.EXPECT().Resource(gomock.Any()).Return(mockNamespaceableResourceInterface)
	unstructuredObj := &unstructured.Unstructured{
		Object: map[string]any{
			"spec": map[string]any{
				"versions": []any{
					map[string]any{
						"name":   "v1beta1",
						"served": true,
					},
					map[string]any{
						"name":   "v1beta2",
						"served": true,
					},
				},
			},
			"status": map[string]any{
				"conditions": []any{
					map[string]any{
						"type":   "Established",
						"status": "True",
					},
				},
			},
		},
	}
	mockNamespaceableResourceInterface.EXPECT().Get(gomock.Any(), testSupportedCRDResourceName, gomock.Any()).Return(unstructuredObj, nil)

	// WHEN
	ok, err := checkCRDExistsAndEstablished(context.Background(), mockDynamicClient, testSupportedCRD)

	// THEN
	require.NoError(t, err)
	assert.False(t, ok)
}

func TestModule_CRDMultipleVersionsSupportNotServed(t *testing.T) {
	// GIVEN
	ctrl := gomock.NewController(t)
	mockDynamicClient := mock_k8s.NewMockInterface(ctrl)
	mockNamespaceableResourceInterface := mock_k8s.NewMockNamespaceableResourceInterface(ctrl)

	// setup mock expectations
	mockDynamicClient.EXPECT().Resource(gomock.Any()).Return(mockNamespaceableResourceInterface)
	unstructuredObj := &unstructured.Unstructured{
		Object: map[string]any{
			"spec": map[string]any{
				"versions": []any{
					map[string]any{
						"name":   "v1beta1",
						"served": true,
					},
					map[string]any{
						"name":   "v1beta2",
						"served": true,
					},
					map[string]any{
						"name":   testSupportedCRDVersion,
						"served": false,
					},
				},
			},
			"status": map[string]any{
				"conditions": []any{
					map[string]any{
						"type":   conditionTypeEstablished,
						"status": conditionStatusTrue,
					},
				},
			},
		},
	}
	mockNamespaceableResourceInterface.EXPECT().Get(gomock.Any(), testSupportedCRDResourceName, gomock.Any()).Return(unstructuredObj, nil)

	// WHEN
	ok, err := checkCRDExistsAndEstablished(context.Background(), mockDynamicClient, testSupportedCRD)

	// THEN
	require.NoError(t, err)
	assert.False(t, ok)
}

func TestModule_CRDMultipleVersionsSupportedAndServedAndEstablished(t *testing.T) {
	// GIVEN
	ctrl := gomock.NewController(t)
	mockDynamicClient := mock_k8s.NewMockInterface(ctrl)
	mockNamespaceableResourceInterface := mock_k8s.NewMockNamespaceableResourceInterface(ctrl)

	// setup mock expectations
	mockDynamicClient.EXPECT().Resource(gomock.Any()).Return(mockNamespaceableResourceInterface)
	unstructuredObj := &unstructured.Unstructured{
		Object: map[string]any{
			"spec": map[string]any{
				"versions": []any{
					map[string]any{
						"name":   "v1beta1",
						"served": true,
					},
					map[string]any{
						"name":   "v1beta2",
						"served": true,
					},
					map[string]any{
						"name":   testSupportedCRDVersion,
						"served": true,
					},
				},
			},
			"status": map[string]any{
				"conditions": []any{
					map[string]any{
						"type":   conditionTypeEstablished,
						"status": conditionStatusTrue,
					},
				},
			},
		},
	}
	mockNamespaceableResourceInterface.EXPECT().Get(gomock.Any(), testSupportedCRDResourceName, gomock.Any()).Return(unstructuredObj, nil)

	// WHEN
	ok, err := checkCRDExistsAndEstablished(context.Background(), mockDynamicClient, testSupportedCRD)

	// THEN
	require.NoError(t, err)
	assert.True(t, ok)
}

func TestModule_CRDNotEstablished(t *testing.T) {
	// GIVEN
	ctrl := gomock.NewController(t)
	mockDynamicClient := mock_k8s.NewMockInterface(ctrl)
	mockNamespaceableResourceInterface := mock_k8s.NewMockNamespaceableResourceInterface(ctrl)

	// setup mock expectations
	mockDynamicClient.EXPECT().Resource(gomock.Any()).Return(mockNamespaceableResourceInterface)
	unstructuredObj := &unstructured.Unstructured{
		Object: map[string]any{
			"spec": map[string]any{
				"versions": []any{
					map[string]any{
						"name":   testSupportedCRDVersion,
						"served": true,
					},
				},
			},
		},
	}
	mockNamespaceableResourceInterface.EXPECT().Get(gomock.Any(), testSupportedCRDResourceName, gomock.Any()).Return(unstructuredObj, nil)

	// WHEN
	ok, err := checkCRDExistsAndEstablished(context.Background(), mockDynamicClient, testSupportedCRD)

	// THEN
	require.NoError(t, err)
	assert.False(t, ok)
}

func TestModule_CRDNotEstablishedBecauseOfWrongCondition(t *testing.T) {
	// GIVEN
	ctrl := gomock.NewController(t)
	mockDynamicClient := mock_k8s.NewMockInterface(ctrl)
	mockNamespaceableResourceInterface := mock_k8s.NewMockNamespaceableResourceInterface(ctrl)

	// setup mock expectations
	mockDynamicClient.EXPECT().Resource(gomock.Any()).Return(mockNamespaceableResourceInterface)
	unstructuredObj := &unstructured.Unstructured{
		Object: map[string]any{
			"spec": map[string]any{
				"versions": []any{
					map[string]any{
						"name":   testSupportedCRDVersion,
						"served": true,
					},
				},
			},
			"status": map[string]any{
				"conditions": []any{
					map[string]any{
						"type":   "NamesAccepted",
						"status": conditionStatusTrue,
					},
				},
			},
		},
	}
	mockNamespaceableResourceInterface.EXPECT().Get(gomock.Any(), testSupportedCRDResourceName, gomock.Any()).Return(unstructuredObj, nil)

	// WHEN
	ok, err := checkCRDExistsAndEstablished(context.Background(), mockDynamicClient, testSupportedCRD)

	// THEN
	require.NoError(t, err)
	assert.False(t, ok)
}

func TestModule_CRDNotEstablishedBecauseOfWrongConditionStatus(t *testing.T) {
	// GIVEN
	ctrl := gomock.NewController(t)
	mockDynamicClient := mock_k8s.NewMockInterface(ctrl)
	mockNamespaceableResourceInterface := mock_k8s.NewMockNamespaceableResourceInterface(ctrl)

	// setup mock expectations
	mockDynamicClient.EXPECT().Resource(gomock.Any()).Return(mockNamespaceableResourceInterface)
	unstructuredObj := &unstructured.Unstructured{
		Object: map[string]any{
			"spec": map[string]any{
				"versions": []any{
					map[string]any{
						"name":   testSupportedCRDVersion,
						"served": true,
					},
				},
			},
			"status": map[string]any{
				"conditions": []any{
					map[string]any{
						"type":   conditionTypeEstablished,
						"status": "False",
					},
				},
			},
		},
	}
	mockNamespaceableResourceInterface.EXPECT().Get(gomock.Any(), testSupportedCRDResourceName, gomock.Any()).Return(unstructuredObj, nil)

	// WHEN
	ok, err := checkCRDExistsAndEstablished(context.Background(), mockDynamicClient, testSupportedCRD)

	// THEN
	require.NoError(t, err)
	assert.False(t, ok)
}

func TestModule_SuccessfullyEstablishedCRD(t *testing.T) {
	// GIVEN
	ctrl := gomock.NewController(t)
	mockDynamicClient := mock_k8s.NewMockInterface(ctrl)
	mockNamespaceableResourceInterface := mock_k8s.NewMockNamespaceableResourceInterface(ctrl)

	// setup mock expectations
	mockDynamicClient.EXPECT().Resource(gomock.Any()).Return(mockNamespaceableResourceInterface)
	unstructuredObj := &unstructured.Unstructured{
		Object: map[string]any{
			"spec": map[string]any{
				"versions": []any{
					map[string]any{
						"name":   testSupportedCRDVersion,
						"served": true,
					},
				},
			},
			"status": map[string]any{
				"conditions": []any{
					map[string]any{
						"type":   conditionTypeEstablished,
						"status": conditionStatusTrue,
					},
				},
			},
		},
	}
	mockNamespaceableResourceInterface.EXPECT().Get(gomock.Any(), testSupportedCRDResourceName, gomock.Any()).Return(unstructuredObj, nil)

	// WHEN
	ok, err := checkCRDExistsAndEstablished(context.Background(), mockDynamicClient, testSupportedCRD)

	// THEN
	require.NoError(t, err)
	assert.True(t, ok)
}
