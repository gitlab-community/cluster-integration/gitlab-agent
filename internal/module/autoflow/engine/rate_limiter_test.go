package engine

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/testlogger"
	"go.uber.org/mock/gomock"
)

func TestRateLimiter_Key(t *testing.T) {
	// GIVEN
	ctrl := gomock.NewController(t)
	mockAllowLimiter := NewMockAllowLimiter(ctrl)
	rl := workflowRateLimiter{al: mockAllowLimiter}

	expectedKey := []byte("42:any-event-type")

	// THEN
	mockAllowLimiter.EXPECT().Allow(gomock.Any()).Do(func(ctx context.Context) bool {
		rlAPI := &workflowRateLimiterAPI{
			log:                   testlogger.New(t),
			handleProcessingError: func(msg string, err error) {},
			requestCtx:            ctx,
		}

		actualKey := rlAPI.RequestKey()

		assert.Equal(t, expectedKey, actualKey)

		return true
	})

	// WHEN
	rl.Allow(context.Background(), 42, "any-event-type")
}
