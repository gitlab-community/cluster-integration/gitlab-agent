package flow

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMasking_markSensitiveGitLabValues(t *testing.T) {
	tests := []struct {
		name              string
		msg               string
		expectedMaskedMsg string
	}{
		{
			name:              "PAT",
			msg:               "This is a glpat-12345678912345678912.",
			expectedMaskedMsg: "This is a [MASKED].",
		},
		{
			name:              "PAT partitioned",
			msg:               "This is a glpat-abcdefghiABCDEFGHI123456789.00abcdefg.",
			expectedMaskedMsg: "This is a [MASKED].",
		},
		{
			name:              "Feed Token feed_token=",
			msg:               "This is a feed_token=12345678901234567890.",
			expectedMaskedMsg: "This is a [MASKED].",
		},
		{
			name:              "Feed Token glft-",
			msg:               "This is a glft-12345678901234567890.",
			expectedMaskedMsg: "This is a [MASKED].",
		},
		{
			name:              "OAuth Application Secret",
			msg:               "This is a gloas-abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789ab.",
			expectedMaskedMsg: "This is a [MASKED].",
		},
		{
			name:              "Deploy Token",
			msg:               "This is a gldt-12345678901234567890.",
			expectedMaskedMsg: "This is a [MASKED].",
		},
		{
			name:              "SCIM OAuth Access Token",
			msg:               "This is a glsoat-12345678901234567890.",
			expectedMaskedMsg: "This is a [MASKED].",
		},
		{
			name:              "CI Build (Job) Token",
			msg:               "This is a glcbt-cb123_12345678901234567890.",
			expectedMaskedMsg: "This is a [MASKED].",
		},
		{
			name:              "Feature Flags Client Token",
			msg:               "This is a glffct-12345678901234567890.",
			expectedMaskedMsg: "This is a [MASKED].",
		},
		{
			name:              "Runner Token",
			msg:               "This is a glrt-12345678901234567890.",
			expectedMaskedMsg: "This is a [MASKED].",
		},
		{
			name:              "Incoming Mail Token",
			msg:               "This is a glimt-1234567890123456789012345.",
			expectedMaskedMsg: "This is a [MASKED].",
		},
		{
			name:              "GitLab Agent Token",
			msg:               "This is a glagent-12345678901234567890123456789012345678901234567890.",
			expectedMaskedMsg: "This is a [MASKED].",
		},
		{
			name:              "Pipeline Trigger Token",
			msg:               "This is a glptt-1234567890123456789012345678901234567890.",
			expectedMaskedMsg: "This is a [MASKED].",
		},
		{
			name:              "Antropic key",
			msg:               "This is a sk-ant-abc12-abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwx-abcdefgh.",
			expectedMaskedMsg: "This is a [MASKED].",
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			actualMaskedMsg := maskSensitiveGitLabValues(tc.msg)
			assert.Equal(t, tc.expectedMaskedMsg, actualMaskedMsg)
		})
	}
}
