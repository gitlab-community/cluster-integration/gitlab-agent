package flow

import "time"

type Builtins interface {
	Print(msg string)
	Now() time.Time
}
