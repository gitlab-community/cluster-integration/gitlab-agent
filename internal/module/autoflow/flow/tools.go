package flow

import (
	"fmt"
	"net/http"
	"net/url"

	"go.starlark.net/starlark"
)

func httpHeaderToDict(h http.Header) (*starlark.Dict, error) {
	d := starlark.NewDict(len(h))

	for k, v := range h {
		vals := make([]starlark.Value, 0, len(v))
		for _, val := range v {
			vals = append(vals, starlark.String(val))
		}
		err := d.SetKey(starlark.String(k), starlark.NewList(vals))
		if err != nil {
			return nil, err
		}
	}

	d.Freeze()
	return d, nil
}

func dictToHTTPHeader(d *starlark.Dict) (http.Header, error) {
	if d == nil || d.Len() == 0 {
		return make(http.Header), nil
	}
	// TODO Use more efficient iteration if/when available https://github.com/google/starlark-go/issues/518
	items := d.Items()
	res := make(http.Header, len(items))
	for _, item := range items {
		key, ok := item[0].(starlark.String)
		if !ok {
			return nil, fmt.Errorf("key must be a string, got %T", item[0])
		}

		var resVal []string
		switch val := item[1].(type) {
		case *starlark.List:
			l := val.Len()
			resVal = make([]string, 0, l)
			for i := 0; i < l; i++ {
				listItem := val.Index(i)
				listItemStr, ok := listItem.(starlark.String)
				if !ok {
					return nil, fmt.Errorf("value must be a string, got %T", listItem)
				}
				resVal = append(resVal, string(listItemStr))
			}
		case starlark.String:
			resVal = []string{string(val)}
		default:
			return nil, fmt.Errorf("value must be a string or a list of strings, got %T", item[1])
		}
		res[string(key)] = resVal
	}

	return res, nil
}

func dictToURLValues(d *starlark.Dict) (url.Values, error) {
	if d == nil || d.Len() == 0 {
		return nil, nil
	}
	// TODO Use more efficient iteration if/when available https://github.com/google/starlark-go/issues/518
	items := d.Items()
	res := make(url.Values, len(items))
	for _, item := range items {
		key, ok := item[0].(starlark.String)
		if !ok {
			return nil, fmt.Errorf("key must be a string, got %T", item[0])
		}

		var resVal []string
		var err error
		switch val := item[1].(type) { // check for concrete types as we don't want to allow Dict as Sequence
		case *starlark.Set:
			resVal, err = sequenceToStringSlice(val)
			if err != nil {
				return nil, err
			}
		case *starlark.List:
			resVal, err = sequenceToStringSlice(val)
			if err != nil {
				return nil, err
			}
		case starlark.Tuple:
			resVal, err = sequenceToStringSlice(val)
			if err != nil {
				return nil, err
			}
		case starlark.String:
			resVal = []string{string(val)}
		default:
			return nil, fmt.Errorf("value must be a string or a list of strings, got %T", item[1])
		}
		res[string(key)] = resVal
	}

	return res, nil
}

func sequenceToStringSlice(s starlark.Sequence) ([]string, error) {
	l := s.Len()
	res := make([]string, 0, l)
	iter := s.Iterate()
	defer iter.Done()
	var item starlark.Value
	for iter.Next(&item) {
		itemStr, ok := item.(starlark.String)
		if !ok {
			return nil, fmt.Errorf("value must be a string, got %T", item)
		}
		res = append(res, string(itemStr))
	}
	return res, nil
}
