package flow

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestParseModuleRef(t *testing.T) {
	tests := []struct {
		rootPath     string
		module       string
		expectedName string
		expectedFile string
	}{
		{
			module:       "abc/file.txt",
			expectedFile: "abc/file.txt",
		},
		{
			module:       "/abc/file.txt",
			expectedFile: "abc/file.txt",
		},
		{
			module:       "@name/abc/file.txt",
			expectedName: "name",
			expectedFile: "abc/file.txt",
		},
		{
			rootPath:     "xyz",
			module:       "abc/file.txt",
			expectedFile: "xyz/abc/file.txt",
		},
		{
			rootPath:     "xyz",
			module:       "/abc/file.txt",
			expectedFile: "abc/file.txt",
		},
		{
			rootPath:     "xyz",
			module:       "@name/abc/file.txt",
			expectedName: "name",
			expectedFile: "abc/file.txt",
		},
	}
	for _, tc := range tests {
		t.Run(tc.module, func(t *testing.T) {
			ref, err := parseModuleRef(tc.rootPath, tc.module)
			require.NoError(t, err)
			assert.Equal(t, tc.expectedName, ref.SourceName)
			assert.Equal(t, tc.expectedFile, ref.File)
		})
	}
}
