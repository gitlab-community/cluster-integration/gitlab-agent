package flow

type TestingSource = preloadedSource

func NewTestingSource(modules []ModuleSource) *TestingSource {
	return &TestingSource{
		modules: modules,
	}
}
