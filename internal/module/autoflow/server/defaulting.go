package server

import (
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/prototool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/pkg/kascfg"
)

const (
	// defaultWorkflowsPerProjectIDEventTypePerMinute defines the default allowed workflow runs per project and event type per minute.
	defaultWorkflowsPerProjectIDEventTypePerMinute = 20
)

func ApplyDefaults(config *kascfg.ConfigurationFile) {
	if config.Autoflow == nil { // module disabled. This is temporary.
		return
	}
	prototool.NotNil(&config.Autoflow.Temporal)
	prototool.NotNil(&config.Autoflow.HttpClient)
	//t := config.Automation.Temporal
	//
	prototool.DefaultValPtr(&config.Autoflow.Temporal.Namespace, "default")

	prototool.DefaultVal(&config.Autoflow.WorkflowsPerProjectIdEventTypePerMinute, defaultWorkflowsPerProjectIDEventTypePerMinute)
	//prototool.DefaultValPtr(&t.CaCertificateFile, "")
	//prototool.DefaultValPtr(&t.CertificateFile, "")
	//prototool.DefaultValPtr(&t.KeyFile, "")
}
