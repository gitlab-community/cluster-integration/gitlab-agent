package encryption

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"go.temporal.io/sdk/converter"
)

var (
	_ converter.PayloadCodec = (*Codec)(nil)

	// insecureKeyForTesting insecure key for testing purposes
	insecureKeyForTesting = []byte("this-secret-key-is-not-secure-01")
)

func TestCodec_SuccessfulEncryption(t *testing.T) {
	// GIVEN
	c := Codec{secretKey: insecureKeyForTesting}
	data := []byte("this-is-any-workflow-data")

	// WHEN
	encryptedData, err := c.encrypt(data)
	require.NoError(t, err)

	unencryptedData, err := c.decrypt(encryptedData)
	require.NoError(t, err)

	// THEN
	assert.Equal(t, data, unencryptedData)
}
