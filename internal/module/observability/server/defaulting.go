package server

import (
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/prototool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/pkg/kascfg"
)

const (
	defaultObservabilityListenNetwork         = "tcp"
	defaultObservabilityListenAddress         = "127.0.0.1:8151"
	defaultObservabilityPrometheusURLPath     = "/metrics"
	defaultObservabilityLivenessProbeURLPath  = "/liveness"
	defaultObservabilityReadinessProbeURLPath = "/readiness"

	defaultGRPCLogLevel = kascfg.LogLevelEnum_error
)

func ApplyDefaults(config *kascfg.ConfigurationFile) {
	prototool.NotNil(&config.Observability)
	o := config.Observability

	prototool.NotNil(&o.Listen)
	prototool.DefaultValPtr(&o.Listen.Network, defaultObservabilityListenNetwork)
	prototool.DefaultVal(&o.Listen.Address, defaultObservabilityListenAddress)

	prototool.NotNil(&o.Prometheus)
	prototool.DefaultVal(&o.Prometheus.UrlPath, defaultObservabilityPrometheusURLPath)

	prototool.NotNil(&o.Sentry)

	prototool.NotNil(&o.Logging)
	if o.Logging.GrpcLevel == nil {
		x := defaultGRPCLogLevel
		o.Logging.GrpcLevel = &x
	}

	prototool.NotNil(&o.LivenessProbe)
	prototool.DefaultVal(&o.LivenessProbe.UrlPath, defaultObservabilityLivenessProbeURLPath)

	prototool.NotNil(&o.ReadinessProbe)
	prototool.DefaultVal(&o.ReadinessProbe.UrlPath, defaultObservabilityReadinessProbeURLPath)
}
