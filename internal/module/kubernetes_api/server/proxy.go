package server

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log/slog"
	"mime"
	"net"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/bufbuild/protovalidate-go"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/api"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/gitlab"
	gapi "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/gitlab/api"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/event_tracker"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/kubernetes_api"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/kubernetes_api/rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/kubernetes_api/server/watch_aggregator"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/usage_metrics"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/cache"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/fieldz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/grpctool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/httpz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/memz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/version"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/vendored/k8s.io/apiserver/pkg/endpoints/handlers/negotiation"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/pkg/agentcfg"
	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
	otelmetric "go.opentelemetry.io/otel/metric"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/trace"
	"go.opentelemetry.io/otel/trace/noop"
	"google.golang.org/protobuf/proto"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
)

const (
	readHeaderTimeout = 10 * time.Second
	idleTimeout       = 1 * time.Minute

	gitLabKASCookieName             = "_gitlab_kas"
	authorizationHeaderBearerPrefix = "Bearer " // must end with a space
	tokenSeparator                  = ":"
	tokenTypeCI                     = "ci"
	tokenTypePat                    = "pat"

	webSocketTokenSubProtocolPrefix = "websocket-token.kas.gitlab.com."

	watchAPIPath = "/watch"

	// See https://w3c.github.io/network-error-logging/#example-2
	nelRemovePoliciesPayload = `{"max_age": 0}`

	// See https://fetch.spec.whatwg.org/#x-content-type-options-header
	xContentTypeOptionsNoSniff = "nosniff"
)

var (
	code2reason = map[int32]metav1.StatusReason{
		// 4xx
		http.StatusBadRequest:            metav1.StatusReasonBadRequest,
		http.StatusUnauthorized:          metav1.StatusReasonUnauthorized,
		http.StatusForbidden:             metav1.StatusReasonForbidden,
		http.StatusNotFound:              metav1.StatusReasonNotFound,
		http.StatusMethodNotAllowed:      metav1.StatusReasonMethodNotAllowed,
		http.StatusNotAcceptable:         metav1.StatusReasonNotAcceptable,
		http.StatusConflict:              metav1.StatusReasonConflict,
		http.StatusGone:                  metav1.StatusReasonGone,
		http.StatusRequestEntityTooLarge: metav1.StatusReasonRequestEntityTooLarge,
		http.StatusUnsupportedMediaType:  metav1.StatusReasonUnsupportedMediaType,
		http.StatusUnprocessableEntity:   metav1.StatusReasonInvalid,
		http.StatusTooManyRequests:       metav1.StatusReasonTooManyRequests,

		// 5xx
		http.StatusInternalServerError: metav1.StatusReasonInternalError,
		http.StatusBadGateway:          metav1.StatusReasonInternalError,
		http.StatusServiceUnavailable:  metav1.StatusReasonServiceUnavailable,
		http.StatusGatewayTimeout:      metav1.StatusReasonTimeout,
	}

	allowedResponseContentTypes = []string{
		runtime.ContentTypeJSON,
		runtime.ContentTypeYAML,
		runtime.ContentTypeProtobuf,
		runtime.ContentTypeCBOR,
		"text/plain",
	}
)

type proxyUserCacheKey struct {
	agentID    int64
	accessType string
	accessKey  string
	csrfToken  string
}

type K8sAPIProxyRequestsEvent struct {
	UserID    int64 `json:"user_id"`
	ProjectID int64 `json:"project_id"`
}

func (e K8sAPIProxyRequestsEvent) DeduplicateKey() string {
	return fmt.Sprintf("%d-%d", e.UserID, e.ProjectID)
}

type webSocketTokenResponse struct {
	Token string `json:"token"`
}

type kubernetesAPIProxy struct {
	log                      *slog.Logger
	api                      modserver.API
	makeRequest              func(ctx context.Context, agentID int64) (grpctool.HTTPRequestClient, error)
	gitLabClient             gitlab.ClientInterface
	allowedOriginURLs        []string
	allowedAgentsCache       *cache.CacheWithErr[string, *gapi.AllowedAgentsForJob]
	authorizeProxyUserCache  *cache.CacheWithErr[proxyUserCacheKey, *gapi.AuthorizeProxyUserResponse]
	requestCounter           usage_metrics.Counter
	ciAccessRequestCounter   usage_metrics.Counter
	ciAccessAgentsCounter    usage_metrics.UniqueCounter
	ciAccessEventTracker     event_tracker.EventsInterface
	userAccessRequestCounter usage_metrics.Counter
	userAccessAgentsCounter  usage_metrics.UniqueCounter
	userAccessEventTracker   event_tracker.EventsInterface
	patAccessRequestCounter  usage_metrics.Counter
	patAccessAgentsCounter   usage_metrics.UniqueCounter
	patAccessEventTracker    event_tracker.EventsInterface
	responseSerializer       runtime.NegotiatedSerializer
	traceProvider            trace.TracerProvider
	tracePropagator          propagation.TextMapPropagator
	meterProvider            otelmetric.MeterProvider
	validator                protovalidate.Validator
	aggregatedWatchCounter   otelmetric.Int64UpDownCounter
	serverName               string
	serverVersion            version.Version
	serverVia                string
	// urlPathPrefix is guaranteed to end with / by defaulting.
	urlPathPrefix       string
	listenerGracePeriod time.Duration
	shutdownGracePeriod time.Duration
	gitlabReleasesList  []string
	httpServerTracing   bool
	webSocketToken      *webSocketToken // NOTE: If `nil` the WebSocket Token support is disabled. This acts as a temporary feature flag mechanism until the feature is completed.
}

func (p *kubernetesAPIProxy) Run(ctx context.Context, listener net.Listener) error {
	var handler http.Handler
	handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		auxCtx, auxCancel := context.WithCancel(ctx)
		defer auxCancel()
		p.proxy(auxCtx, w, r)
	})
	var tp trace.TracerProvider
	if p.httpServerTracing {
		tp = p.traceProvider
	} else {
		tp = noop.NewTracerProvider()
	}
	handler = otelhttp.NewHandler(handler, "k8s-proxy",
		otelhttp.WithTracerProvider(tp),
		otelhttp.WithPropagators(p.tracePropagator),
		otelhttp.WithMeterProvider(p.meterProvider),
		otelhttp.WithPublicEndpoint(),
	)
	srv := &http.Server{
		Handler:           handler,
		ReadHeaderTimeout: readHeaderTimeout,
		IdleTimeout:       idleTimeout,
	}
	return httpz.RunServerWithUpgradeSupport(ctx, srv, listener, p.listenerGracePeriod, p.shutdownGracePeriod)
}

// proxy Kubernetes API calls via agentk to the cluster Kube API.
//
// This method also takes care of CORS preflight requests as documented [here](https://developer.mozilla.org/en-US/docs/Glossary/Preflight_request).
func (p *kubernetesAPIProxy) proxy(auxCtx context.Context, w http.ResponseWriter, r *http.Request) {
	// for preflight and normal requests we want to allow some configured allowed origins and
	// support exposing the response to the client when credentials (e.g. cookies) are included in the request
	header := w.Header()

	requestedOrigin := r.Header.Get(httpz.OriginHeader)
	if requestedOrigin != "" {
		// If the Origin header is set, it needs to match the configured allowed origin urls.
		if !p.isOriginAllowed(requestedOrigin) {
			// Reject the request because origin is not allowed
			if p.log.Enabled(context.Background(), slog.LevelDebug) { //nolint: contextcheck
				p.log.Debug(fmt.Sprintf("Received Origin %q is not in configured allowed origins", requestedOrigin))
			}
			w.WriteHeader(http.StatusForbidden)
			return
		}
		header[httpz.AccessControlAllowOriginHeader] = []string{requestedOrigin}
		header[httpz.AccessControlAllowCredentialsHeader] = []string{"true"}
		header[httpz.VaryHeader] = []string{httpz.OriginHeader}
	}
	header[httpz.ServerHeader] = []string{p.serverName} // It will be removed just before responding with actual headers from upstream

	if r.Method == http.MethodOptions {
		// we have a preflight request
		header[httpz.AccessControlAllowHeadersHeader] = r.Header[httpz.AccessControlRequestHeadersHeader]
		// all allowed HTTP methods:
		// see https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods
		header[httpz.AccessControlAllowMethodsHeader] = []string{"GET, HEAD, POST, PUT, DELETE, CONNECT, OPTIONS, TRACE, PATCH"}
		header[httpz.AccessControlMaxAgeHeader] = []string{"86400"}
		return
	}

	ctx := r.Context()
	log := p.log.With(logz.TraceIDFromContext(ctx)) //nolint:contextcheck

	if !strings.HasPrefix(r.URL.Path, p.urlPathPrefix) {
		msg := "Bad request: URL does not start with expected prefix"
		log.Debug(msg, logz.URLPath(r.URL.Path), logz.URLPathPrefix(p.urlPathPrefix))
		p.writeErrorResponse(log, modshared.NoAgentID, w, r)(&grpctool.ErrResp{ //nolint:contextcheck
			StatusCode: http.StatusBadRequest,
			Msg:        msg,
		})
		return
	}

	// authenticate proxy request
	log, agentID, userID, impConfig, creds, eResp := p.authenticateAndImpersonateRequest(ctx, log, r) //nolint:contextcheck
	if eResp != nil {
		// If GitLab doesn't authorize the proxy user to make the call,
		// we send an extra header to indicate that, so that the client
		// can differentiate from an *unauthorized* response from GitLab
		// and from an *authorized* response from the proxied K8s cluster.
		if eResp.StatusCode == http.StatusUnauthorized {
			header[httpz.GitlabUnauthorizedHeader] = []string{"true"}
		}
		p.writeErrorResponse(log, agentID, w, r)(eResp) //nolint:contextcheck
		return
	}

	query := r.URL.Query()

	if p.webSocketToken != nil {
		// check for WebSocket token requests
		if r.Method == http.MethodGet && isSessionCookieAuthn(creds) && query.Has(webSocketTokenRequestParamName) {
			url := fmt.Sprintf("%s/%s", r.Host, strings.TrimPrefix(r.URL.Path, "/"))
			token, err := p.webSocketToken.generate(url, agentID, userID, impConfig)
			if err != nil {
				p.api.HandleProcessingError(ctx, log, "Failed to generate WebSocket token", err) //nolint:contextcheck
				p.writeErrorResponse(log, agentID, w, r)(&grpctool.ErrResp{                      //nolint:contextcheck
					StatusCode: http.StatusInternalServerError,
					Msg:        "Failed to generate WebSocket token",
				})
				return
			}

			header[httpz.ContentTypeHeader] = []string{"application/json"}
			b, err := json.Marshal(webSocketTokenResponse{Token: token})
			if err != nil {
				p.writeErrorResponse(log, agentID, w, r)(&grpctool.ErrResp{ //nolint:contextcheck
					StatusCode: http.StatusInternalServerError,
					Msg:        "Failed to marshal WebSocket token response",
					Err:        err,
				})
				return
			}
			_, _ = w.Write(b) // IO errors
			return
		}
	}

	// prepare request for proxying

	// urlPathPrefix is guaranteed to end with / by defaulting. That means / will be removed here.
	// Put it back by -1 on length.
	r.URL.Path = r.URL.Path[len(p.urlPathPrefix)-1:]

	// remove GitLab authorization headers (job token, session cookie etc)
	delete(r.Header, httpz.AuthorizationHeader)
	delete(r.Header, httpz.CookieHeader)
	delete(r.Header, httpz.GitlabAgentIDHeader)
	delete(r.Header, httpz.CSRFTokenHeader)
	// remove websocket token auth information
	httpz.RemoveHeaderValue(
		r.Header,
		httpz.SecWebSocketProtocolHeader,
		func(s string) bool { return strings.HasPrefix(s, webSocketTokenSubProtocolPrefix) },
	)
	// remove GitLab authorization query parameters
	delete(query, httpz.GitlabAgentIDQueryParam)
	delete(query, httpz.CSRFTokenQueryParam)
	r.URL.RawQuery = query.Encode()
	r.Header[httpz.ViaHeader] = append(r.Header[httpz.ViaHeader], p.serverVia)

	if r.URL.Path == watchAPIPath {
		p.proxyAggregatedWatch(auxCtx, log, agentID, impConfig, w, r)
		return
	}

	abort := p.proxyRequest(log, agentID, impConfig, creds, w, r) //nolint:contextcheck
	if abort {
		// we need to use panic here for proper status code handling.
		panic(http.ErrAbortHandler)
	}
}

func (p *kubernetesAPIProxy) isOriginAllowed(origin string) bool {
	for _, v := range p.allowedOriginURLs {
		if v == origin {
			return true
		}
	}
	return false
}

func (p *kubernetesAPIProxy) proxyRequest(log *slog.Logger, agentID int64, impConfig *kubernetes_api.ImpersonationConfig,
	creds any, w http.ResponseWriter, r *http.Request) bool {
	p.requestCounter.Inc() // Count only authenticated and authorized requests

	http2grpc := grpctool.InboundHTTPToOutboundGRPC{
		HTTP2GRPC: grpctool.HTTPToOutboundGRPC{
			Log: log,
			HandleProcessingErrorFunc: func(msg string, err error) {
				p.api.HandleProcessingError(r.Context(), log, msg, err, fieldz.AgentID(agentID))
			},
			CheckHeader: func(statusCode int32, header http.Header) error {
				if !isBrowserAuthn(creds) {
					// The below checks only apply to requests that authenticate using a cookie.
					return nil
				}
				if isHTTPRedirectStatusCode(statusCode) {
					// Do not proxy redirects for requests with Cookie + CSRF token authentication.
					return errors.New("redirects are not allowed")
				}
				err := checkContentType(header, allowedResponseContentTypes...)
				if err != nil {
					return err
				}

				if len(header[httpz.SetCookieHeader]) > 0 {
					delete(header, httpz.SetCookieHeader)
					log.Debug("Deleted Set-Cookie from the server's response")
				}
				return nil
			},
		},
		NewClient: func(ctx context.Context) (grpctool.HTTPRequestClient, error) {
			return p.makeRequest(ctx, agentID)
		},
		WriteErrorResponse: p.writeErrorResponse(log, agentID, w, r),
		MergeHeaders:       p.mergeProxiedResponseHeaders,
	}
	return http2grpc.Pipe(w, r, impConfig2extra(impConfig))
}

func (p *kubernetesAPIProxy) proxyAggregatedWatch(auxCtx context.Context, log *slog.Logger, agentID int64, impConfig *kubernetes_api.ImpersonationConfig, w http.ResponseWriter, r *http.Request) {
	wa := watch_aggregator.NewWatchAggregator(
		log,
		p.api,
		p.validator,
		func() (watch_aggregator.WebSocketInterface, error) {
			return watch_aggregator.WebSocketAccept(w, r, p.allowedOriginURLs)
		},
		&grpctool.HTTPRoundTripperToOutboundGRPC{
			HTTP2GRPC: grpctool.HTTPToOutboundGRPC{
				Log: log,
				HandleProcessingErrorFunc: func(msg string, err error) { //nolint:contextcheck
					p.api.HandleProcessingError(r.Context(), log, msg, err, fieldz.AgentID(agentID))
				},
				CheckHeader: func(statusCode int32, header http.Header) error {
					return nil
				},
			},
			NewClient: func(ctx context.Context) (grpctool.HTTPRequestClient, error) {
				return p.makeRequest(ctx, agentID)
			},
			HeaderExtra: impConfig2extra(impConfig),
		},
	)
	if wa == nil {
		// we prematurely closed the websocket connection on a user error
		return
	}

	p.aggregatedWatchCounter.Add(context.Background(), 1)        //nolint:contextcheck
	defer p.aggregatedWatchCounter.Add(context.Background(), -1) //nolint:contextcheck

	// handle aggregated watch requests
	wa.Handle(auxCtx)
}

// Don't return a concrete type or extra will be passed as a typed nil.
func impConfig2extra(impConfig *kubernetes_api.ImpersonationConfig) proto.Message {
	if impConfig == nil {
		return nil
	}
	return &rpc.HeaderExtra{
		ImpConfig: impConfig,
	}
}

func (p *kubernetesAPIProxy) authenticateAndImpersonateRequest(ctx context.Context, log *slog.Logger, r *http.Request) (*slog.Logger, int64 /* agentID */, int64 /* user ID */, *kubernetes_api.ImpersonationConfig, any, *grpctool.ErrResp) {
	agentID, creds, err := p.getAuthorizationInfoFromRequest(r)
	if err != nil {
		msg := "Unauthorized"
		log.Debug(msg, logz.Error(err))
		return log, modshared.NoAgentID, 0, nil, nil, &grpctool.ErrResp{
			StatusCode: http.StatusUnauthorized,
			Msg:        msg,
			Err:        err,
		}
	}
	log = log.With(logz.AgentID(agentID))
	trace.SpanFromContext(ctx).SetAttributes(api.TraceAgentIDAttr.Int64(agentID))

	var (
		userID    int64
		impConfig *kubernetes_api.ImpersonationConfig // can be nil
	)

	switch c := creds.(type) {
	case ciJobTokenAuthn:
		allowedForJob, eResp := p.getAllowedAgentsForJob(ctx, log, agentID, c.jobToken)
		if eResp != nil {
			return log, agentID, 0, nil, creds, eResp
		}
		userID = allowedForJob.User.Id

		aa := findAllowedAgent(agentID, allowedForJob)
		if aa == nil {
			msg := "Forbidden: agentID is not allowed"
			log.Debug(msg)
			return log, agentID, 0, nil, creds, &grpctool.ErrResp{
				StatusCode: http.StatusForbidden,
				Msg:        msg,
			}
		}

		impConfig, err = constructJobImpersonationConfig(allowedForJob, aa)
		if err != nil {
			msg := "Failed to construct impersonation config"
			p.api.HandleProcessingError(ctx, log, msg, err, fieldz.AgentID(agentID))
			return log, agentID, 0, nil, creds, &grpctool.ErrResp{
				StatusCode: http.StatusInternalServerError,
				Msg:        msg,
				Err:        err,
			}
		}

		// update usage metrics for `ci_access` requests using the CI tunnel
		p.ciAccessRequestCounter.Inc()
		p.ciAccessAgentsCounter.Add(agentID)
		p.ciAccessEventTracker.EmitEvent(K8sAPIProxyRequestsEvent{UserID: userID, ProjectID: aa.ConfigProject.Id})
	case sessionCookieAuthn:
		auth, eResp := p.authorizeProxyUser(ctx, log, agentID, "session_cookie", c.encryptedPublicSessionID, c.csrfToken)
		if eResp != nil {
			return log, agentID, 0, nil, creds, eResp
		}
		userID = auth.User.Id
		impConfig, err = constructUserImpersonationConfig(auth, "session_cookie")
		if err != nil {
			msg := "Failed to construct user impersonation config"
			p.api.HandleProcessingError(ctx, log, msg, err, fieldz.AgentID(agentID))
			return log, agentID, 0, nil, creds, &grpctool.ErrResp{
				StatusCode: http.StatusInternalServerError,
				Msg:        msg,
				Err:        err,
			}
		}

		// update usage metrics for `user_access` requests using the CI tunnel
		p.userAccessRequestCounter.Inc()
		p.userAccessAgentsCounter.Add(agentID)
		p.userAccessEventTracker.EmitEvent(K8sAPIProxyRequestsEvent{UserID: userID, ProjectID: auth.Agent.ConfigProject.Id})
	case patAuthn:
		auth, eResp := p.authorizeProxyUser(ctx, log, agentID, "personal_access_token", c.token, "")
		if eResp != nil {
			return log, agentID, 0, nil, creds, eResp
		}
		userID = auth.User.Id
		impConfig, err = constructUserImpersonationConfig(auth, "personal_access_token")
		if err != nil {
			msg := "Failed to construct user impersonation config"
			p.api.HandleProcessingError(ctx, log, msg, err, fieldz.AgentID(agentID))
			return log, agentID, 0, nil, creds, &grpctool.ErrResp{
				StatusCode: http.StatusInternalServerError,
				Msg:        msg,
				Err:        err,
			}
		}

		// update usage metrics for PAT requests using the CI tunnel
		p.patAccessRequestCounter.Inc()
		p.patAccessAgentsCounter.Add(agentID)
		p.patAccessEventTracker.EmitEvent(K8sAPIProxyRequestsEvent{UserID: userID, ProjectID: auth.Agent.ConfigProject.Id})
	case webSocketTokenAuthn:
		impConfig = c.impConfig

		// not going to update any metrics because it was already done via the token request
	default: // This should never happen
		msg := "Invalid authorization type"
		p.api.HandleProcessingError(ctx, log, msg, err, fieldz.AgentID(agentID))
		return log, agentID, 0, nil, creds, &grpctool.ErrResp{
			StatusCode: http.StatusInternalServerError,
			Msg:        msg,
		}
	}
	return log, agentID, userID, impConfig, creds, nil
}

func (p *kubernetesAPIProxy) getAllowedAgentsForJob(ctx context.Context, log *slog.Logger, agentID int64, jobToken string) (*gapi.AllowedAgentsForJob, *grpctool.ErrResp) {
	allowedForJob, err := p.allowedAgentsCache.GetItem(ctx, jobToken, func() (*gapi.AllowedAgentsForJob, error) {
		return gapi.GetAllowedAgentsForJob(ctx, p.gitLabClient, jobToken)
	})
	if err != nil {
		var status int32
		var msg string
		switch {
		case gitlab.IsUnauthorized(err):
			status = http.StatusUnauthorized
			msg = "Unauthorized: CI job token"
			log.Debug(msg, logz.Error(err))
		case gitlab.IsForbidden(err):
			status = http.StatusForbidden
			msg = "Forbidden: CI job token"
			log.Debug(msg, logz.Error(err))
		case gitlab.IsNotFound(err):
			status = http.StatusNotFound
			msg = "Not found: agents for CI job token"
			log.Debug(msg, logz.Error(err))
		default:
			status = http.StatusInternalServerError
			msg = "Failed to get allowed agents for CI job token"
			p.api.HandleProcessingError(ctx, log, msg, err, fieldz.AgentID(agentID))
		}
		return nil, &grpctool.ErrResp{
			StatusCode: status,
			Msg:        msg,
			Err:        err,
		}
	}
	return allowedForJob, nil
}

func (p *kubernetesAPIProxy) authorizeProxyUser(ctx context.Context, log *slog.Logger, agentID int64, accessType, accessKey, csrfToken string) (*gapi.AuthorizeProxyUserResponse, *grpctool.ErrResp) {
	key := proxyUserCacheKey{
		agentID:    agentID,
		accessType: accessType,
		accessKey:  accessKey,
		csrfToken:  csrfToken,
	}
	auth, err := p.authorizeProxyUserCache.GetItem(ctx, key, func() (*gapi.AuthorizeProxyUserResponse, error) {
		return gapi.AuthorizeProxyUser(ctx, p.gitLabClient, agentID, accessType, accessKey, csrfToken)
	})
	if err != nil {
		switch {
		case gitlab.IsUnauthorized(err), gitlab.IsForbidden(err), gitlab.IsNotFound(err), gitlab.IsBadRequest(err):
			log.Debug("Authorize proxy user error", logz.Error(err))
			return nil, &grpctool.ErrResp{
				StatusCode: http.StatusUnauthorized,
				Msg:        "Unauthorized",
			}
		default:
			msg := "Failed to authorize user session"
			p.api.HandleProcessingError(ctx, log, msg, err, fieldz.AgentID(agentID))
			return nil, &grpctool.ErrResp{
				StatusCode: http.StatusInternalServerError,
				Msg:        msg,
			}
		}

	}
	return auth, nil
}

func (p *kubernetesAPIProxy) mergeProxiedResponseHeaders(outbound, inbound http.Header) {
	delete(inbound, httpz.ServerHeader) // remove the header we've added above. We use Via instead.
	// remove all potential CORS headers from the proxied response
	delete(outbound, httpz.AccessControlAllowOriginHeader)
	delete(outbound, httpz.AccessControlAllowHeadersHeader)
	delete(outbound, httpz.AccessControlAllowCredentialsHeader)
	delete(outbound, httpz.AccessControlAllowMethodsHeader)
	delete(outbound, httpz.AccessControlMaxAgeHeader)
	delete(outbound, httpz.NELHeader)
	delete(outbound, httpz.ReportToHeader)

	// set headers from proxied response without overwriting the ones already set (e.g. CORS headers)
	for k, vals := range outbound {
		if len(inbound[k]) == 0 {
			inbound[k] = vals
		}
	}
	// explicitly merge Vary header with the headers from proxies requests.
	// We always set the Vary header to `Origin` for CORS
	if v := append(inbound[httpz.VaryHeader], outbound[httpz.VaryHeader]...); len(v) > 0 {
		inbound[httpz.VaryHeader] = v
	}
	inbound[httpz.ViaHeader] = append(inbound[httpz.ViaHeader], p.serverVia)

	// Set Nel header max-age to 0 to remove any previous NEL configuration
	// see https://w3c.github.io/network-error-logging/#the-max_age-member
	// see https://w3c.github.io/network-error-logging/#example-2 (for example on how to remove existing NEL policies)
	inbound[httpz.NELHeader] = []string{nelRemovePoliciesPayload}

	// Prevent user agents from mime sniffing the response content type
	inbound[httpz.XContentTypeOptionsHeader] = []string{xContentTypeOptionsNoSniff}

	p.addWarningHeaderIfOutdatedAgent(outbound, inbound)
}

func (p *kubernetesAPIProxy) addWarningHeaderIfOutdatedAgent(outbound, inbound http.Header) {
	agentVersionString := outbound.Get(httpz.GitlabAgentVersionHeader)
	if agentVersionString == "" {
		return
	}

	agentVersion, err := version.NewVersion(agentVersionString)
	if err != nil {
		p.log.Debug("Agent version from proxy response header is not valid", logz.Error(err))
		return
	}

	typ, warning := version.WarningIfOutdatedAgent(p.serverVersion, agentVersion, p.gitlabReleasesList)
	switch typ { //nolint:exhaustive
	case version.AgentVersionNoWarning:
		return
	default:
		inbound[httpz.WarningHeader] = []string{fmt.Sprintf("299 - %q", warning)}
	}
}

func (p *kubernetesAPIProxy) writeErrorResponse(log *slog.Logger, agentID int64, w http.ResponseWriter, r *http.Request) grpctool.WriteErrorResponse {
	return func(errResp *grpctool.ErrResp) {
		_, info, err := negotiation.NegotiateOutputMediaType(r, p.responseSerializer, negotiation.DefaultEndpointRestrictions)
		ctx := r.Context()
		if err != nil {
			msg := "Failed to negotiate output media type"
			log.Debug(msg, logz.Error(err))
			http.Error(w, formatStatusMessage(ctx, msg, err), http.StatusNotAcceptable)
			return
		}
		message := formatStatusMessage(ctx, errResp.Msg, errResp.Err)
		s := &metav1.Status{
			TypeMeta: metav1.TypeMeta{
				Kind:       "Status",
				APIVersion: "v1",
			},
			Status:  metav1.StatusFailure,
			Message: message,
			Reason:  code2reason[errResp.StatusCode], // if mapping is not present, then "" means metav1.StatusReasonUnknown
			Code:    errResp.StatusCode,
		}
		bp := memz.Get32k() // use a temporary buffer to segregate I/O errors and encoding errors
		defer memz.Put32k(bp)
		buf := (*bp)[:0] // don't care what's in the buf, start writing from the start
		b := bytes.NewBuffer(buf)
		err = info.Serializer.Encode(s, b) // encoding errors
		if err != nil {
			p.api.HandleProcessingError(ctx, log, "Failed to encode status response", err, fieldz.AgentID(agentID))
			http.Error(w, message, int(errResp.StatusCode))
			return
		}
		w.Header()[httpz.ContentTypeHeader] = []string{info.MediaType}
		w.WriteHeader(int(errResp.StatusCode))
		_, _ = w.Write(b.Bytes()) // I/O errors
	}
}

func isHTTPRedirectStatusCode(statusCode int32) bool {
	return statusCode >= 300 && statusCode < 400
}

// isBrowserAuthn checks if the used creds are browser based.
// browser based:
// - session cookie authentication.
// - websocket token authentication.
// the websocket token authentication is considered a browser based
// authentication because it was ended out by providing
// a valid session cookie which is browser based.
func isBrowserAuthn(creds any) bool {
	switch creds.(type) {
	case sessionCookieAuthn, webSocketTokenAuthn:
		return true
	default:
		return false
	}
}

// isSessionCookieAuthn checks if the used creds is from a session cookie authentication
func isSessionCookieAuthn(creds any) bool {
	_, ok := creds.(sessionCookieAuthn)
	return ok
}

func checkContentType(h http.Header, allowed ...string) error {
	// There should be at most one Content-Type header, but it's not our job to do something about it if there is more.
	// We just ensure thy are all allowed.
nextContentType:
	for _, ct := range h[httpz.ContentTypeHeader] {
		mediatype, _, err := mime.ParseMediaType(ct)
		if err != nil && err != mime.ErrInvalidMediaParameter {
			// Parsing error and not a MIME parameter parsing error, which we ignore.
			return fmt.Errorf("check Content-Type: %w", err)
		}
		for _, a := range allowed {
			if mediatype == a {
				// This one is allowed, onto the next Content-Type header
				continue nextContentType
			}
		}
		return fmt.Errorf("%s not allowed: %s", httpz.ContentTypeHeader, mediatype)
	}
	return nil
}

// err can be nil.
func formatStatusMessage(ctx context.Context, msg string, err error) string {
	var b strings.Builder
	b.WriteString("GitLab Agent Server: ")
	b.WriteString(msg)
	if err != nil {
		b.WriteString(": ")
		b.WriteString(err.Error())
	}
	traceID := trace.SpanContextFromContext(ctx).TraceID()
	if traceID.IsValid() {
		b.WriteString(". Trace ID: ")
		b.WriteString(traceID.String())
	}
	return b.String()
}

func findAllowedAgent(agentID int64, agentsForJob *gapi.AllowedAgentsForJob) *gapi.AllowedAgent {
	for _, aa := range agentsForJob.AllowedAgents {
		if aa.Id == agentID {
			return aa
		}
	}
	return nil
}

type ciJobTokenAuthn struct {
	jobToken string
}

type patAuthn struct {
	token string
}

type sessionCookieAuthn struct {
	encryptedPublicSessionID string
	csrfToken                string
}

type webSocketTokenAuthn struct {
	impConfig *kubernetes_api.ImpersonationConfig
}

func isWebSocketRequest(r *http.Request) bool {
	return httpz.HasHeaderValue(r.Header, httpz.ConnectionHeader, "upgrade") && httpz.HasHeaderValue(r.Header, httpz.UpgradeHeader, "websocket")
}

func (p *kubernetesAPIProxy) getAuthorizationInfoFromRequest(r *http.Request) (int64 /* agentID */, any, error) {
	// NOTE: we intentionally have multiple disconnected ifs in this method to support authentication fallbacks.
	// For example, we may want to authenticate via session cookie with KAS but still send an Authorization header
	// to the Kubernetes API.

	if p.webSocketToken != nil {
		if isWebSocketRequest(r) && len(r.Header[httpz.SecWebSocketProtocolHeader]) > 0 {
			for protocol := range httpz.IterHeaderValues(r.Header, httpz.SecWebSocketProtocolHeader) {
				webSocketToken, found := strings.CutPrefix(protocol, webSocketTokenSubProtocolPrefix)
				if !found {
					continue
				}

				url := fmt.Sprintf("%s/%s", r.Host, strings.TrimPrefix(r.URL.Path, "/"))
				agentID, impConfig, err := p.webSocketToken.verify(webSocketToken, url)
				if err != nil {
					return 0, nil, err
				}

				return agentID, webSocketTokenAuthn{impConfig: impConfig}, nil
			}
		}
	}

	if authzHeader := r.Header[httpz.AuthorizationHeader]; len(authzHeader) >= 1 {
		if len(authzHeader) > 1 {
			return 0, nil, fmt.Errorf("%s header: expecting a single header, got %d", httpz.AuthorizationHeader, len(authzHeader))
		}
		agentID, tokenType, token, err := getAgentIDAndTokenFromHeader(authzHeader[0])
		if err != nil {
			return 0, nil, err
		}
		switch tokenType {
		case tokenTypeCI:
			return agentID, ciJobTokenAuthn{
				jobToken: token,
			}, nil
		case tokenTypePat:
			return agentID, patAuthn{
				token: token,
			}, nil
		}
	}

	if cookie, err := r.Cookie(gitLabKASCookieName); err == nil {
		agentID, encryptedPublicSessionID, csrfToken, err := getSessionCookieParams(cookie, r)
		if err != nil {
			return 0, nil, err
		}
		return agentID, sessionCookieAuthn{
			encryptedPublicSessionID: encryptedPublicSessionID,
			csrfToken:                csrfToken,
		}, nil
	}
	return 0, nil, errors.New("no valid credentials provided")
}

func getSessionCookieParams(cookie *http.Cookie, r *http.Request) (int64, string, string, error) {
	if len(cookie.Value) == 0 {
		return 0, "", "", fmt.Errorf("%s cookie value must not be empty", gitLabKASCookieName)
	}
	// NOTE: GitLab Rails uses `rack` as the generic web server framework, which escapes the cookie values.
	// See https://github.com/rack/rack/blob/0b26518acc4c946ca96dfe3d9e68a05ca84439f7/lib/rack/utils.rb#L300
	encryptedPublicSessionID, err := url.QueryUnescape(cookie.Value)
	if err != nil {
		return 0, "", "", fmt.Errorf("%s invalid cookie value", gitLabKASCookieName)
	}

	agentID, err := getAgentIDForSessionCookieRequest(r)
	if err != nil {
		return 0, "", "", err
	}
	csrfToken, err := getCSRFTokenForSessionCookieRequest(r)
	if err != nil {
		return 0, "", "", err
	}
	return agentID, encryptedPublicSessionID, csrfToken, nil
}

// getAgentIDForSessionCookieRequest retrieves the agent id from the request when trying to authenticate with a session cookie.
// First, the agent id is tried to be retrieved from the headers.
// If that fails, the query parameters are tried.
// When both the agent id is provided in the headers and the query parameters the query parameter
// has precedence and the query parameter is silently ignored.
func getAgentIDForSessionCookieRequest(r *http.Request) (int64, error) {
	parseAgentID := func(agentIDStr string) (int64, error) {
		agentID, err := strconv.ParseInt(agentIDStr, 10, 64)
		if err != nil {
			return 0, fmt.Errorf("agent id in request: invalid value: %q", agentIDStr)
		}
		return agentID, nil
	}
	// Check the agent id header and return if it is present and a valid
	agentIDHeader := r.Header[httpz.GitlabAgentIDHeader]
	if len(agentIDHeader) == 1 {
		return parseAgentID(agentIDHeader[0])
	}

	// If multiple agent id headers are given we abort with a failure
	if len(agentIDHeader) > 1 {
		return 0, fmt.Errorf("%s header must have exactly one value", httpz.GitlabAgentIDHeader)
	}

	// Check the query parameters for a valid agent id
	agentIDParam := r.URL.Query()[httpz.GitlabAgentIDQueryParam]
	if len(agentIDParam) != 1 {
		return 0, fmt.Errorf("exactly one agent id must be provided either in the %q header or %q query parameter", httpz.GitlabAgentIDHeader, httpz.GitlabAgentIDQueryParam)
	}

	return parseAgentID(agentIDParam[0])
}

// getCSRFTokenForSessionCookieRequest retrieves the CSRF token from the request when trying to authenticate with a session cookie.
// First, the CSRF token is tried to be retrieved from the headers.
// If that fails, the query parameters are tried.
// When both the CSRF token is provided in the headers and the query parameters the query parameter
// has precedence and the query parameter is silently ignored.
func getCSRFTokenForSessionCookieRequest(r *http.Request) (string, error) {
	// Check the CSRF token header and return if it is present
	csrfTokenHeader := r.Header[httpz.CSRFTokenHeader]
	if len(csrfTokenHeader) == 1 {
		return csrfTokenHeader[0], nil
	}

	// If multiple CSRF tokens headers are given we abort with a failure
	if len(csrfTokenHeader) > 1 {
		return "", fmt.Errorf("%s header must have exactly one value", httpz.CSRFTokenHeader)
	}

	// Check the query parameters for a valid CSRF token
	csrfTokenParam := r.URL.Query()[httpz.CSRFTokenQueryParam]
	if len(csrfTokenParam) != 1 {
		return "", fmt.Errorf("exactly one CSRF token must be provided either in the %q header or %q query parameter", httpz.CSRFTokenHeader, httpz.CSRFTokenQueryParam)
	}

	return csrfTokenParam[0], nil
}

func getAgentIDAndTokenFromHeader(header string) (int64, string /* token type */, string /* token */, error) {
	if !strings.HasPrefix(header, authorizationHeaderBearerPrefix) {
		// "missing" space in message - it's in the authorizationHeaderBearerPrefix constant already
		return 0, "", "", fmt.Errorf("%s header: expecting %stoken", httpz.AuthorizationHeader, authorizationHeaderBearerPrefix)
	}
	tokenValue := header[len(authorizationHeaderBearerPrefix):]
	tokenType, tokenContents, found := strings.Cut(tokenValue, tokenSeparator)
	if !found {
		return 0, "", "", fmt.Errorf("%s header: invalid value", httpz.AuthorizationHeader)
	}
	switch tokenType {
	case tokenTypeCI:
	case tokenTypePat:
	default:
		return 0, "", "", fmt.Errorf("%s header: unknown token type", httpz.AuthorizationHeader)
	}
	agentIDAndToken := tokenContents
	agentIDStr, token, found := strings.Cut(agentIDAndToken, tokenSeparator)
	if !found {
		return 0, "", "", fmt.Errorf("%s header: invalid value", httpz.AuthorizationHeader)
	}
	agentID, err := strconv.ParseInt(agentIDStr, 10, 64)
	if err != nil {
		return 0, "", "", fmt.Errorf("%s header: failed to parse: %w", httpz.AuthorizationHeader, err)
	}
	if token == "" {
		return 0, "", "", fmt.Errorf("%s header: empty token", httpz.AuthorizationHeader)
	}
	return agentID, tokenType, token, nil
}

func constructJobImpersonationConfig(allowedForJob *gapi.AllowedAgentsForJob, aa *gapi.AllowedAgent) (*kubernetes_api.ImpersonationConfig, error) {
	as := aa.GetConfiguration().GetAccessAs().GetAs() // all these fields are optional, so handle nils.
	switch imp := as.(type) {
	case nil, *agentcfg.CiAccessAsCF_Agent: // nil means default value, which is Agent.
		return nil, nil
	case *agentcfg.CiAccessAsCF_Impersonate:
		i := imp.Impersonate
		return &kubernetes_api.ImpersonationConfig{
			Username: i.Username,
			Groups:   i.Groups,
			Uid:      i.Uid,
			Extra:    impImpersonationExtra(i.Extra),
		}, nil
	case *agentcfg.CiAccessAsCF_CiJob:
		return &kubernetes_api.ImpersonationConfig{
			Username: fmt.Sprintf("gitlab:ci_job:%d", allowedForJob.Job.Id),
			Groups:   impCIJobGroups(allowedForJob),
			Extra:    impCIJobExtra(allowedForJob, aa),
		}, nil
	default:
		// Normally this should never happen
		return nil, fmt.Errorf("unexpected job impersonation mode: %T", imp)
	}
}

func constructUserImpersonationConfig(auth *gapi.AuthorizeProxyUserResponse, accessType string) (*kubernetes_api.ImpersonationConfig, error) {
	switch imp := auth.GetAccessAs().AccessAs.(type) {
	case *gapi.AccessAsProxyAuthorization_Agent:
		return nil, nil
	case *gapi.AccessAsProxyAuthorization_User:
		return &kubernetes_api.ImpersonationConfig{
			Username: fmt.Sprintf("gitlab:user:%s", auth.User.Username),
			Groups:   impUserGroups(imp.User),
			Extra:    impUserExtra(auth, accessType),
		}, nil
	default:
		// Normally this should never happen
		return nil, fmt.Errorf("unexpected user impersonation mode: %T", imp)
	}
}

func impImpersonationExtra(in []*agentcfg.ExtraKeyValCF) []*kubernetes_api.ExtraKeyVal {
	out := make([]*kubernetes_api.ExtraKeyVal, 0, len(in))
	for _, kv := range in {
		out = append(out, &kubernetes_api.ExtraKeyVal{
			Key: kv.Key,
			Val: kv.Val,
		})
	}
	return out
}

func impCIJobGroups(allowedForJob *gapi.AllowedAgentsForJob) []string {
	// 1. gitlab:ci_job to identify all requests coming from CI jobs.
	groups := make([]string, 0, 3+len(allowedForJob.Project.Groups))
	groups = append(groups, "gitlab:ci_job")
	// 2. The list of ids of groups the project is in.
	for _, projectGroup := range allowedForJob.Project.Groups {
		groups = append(groups, fmt.Sprintf("gitlab:group:%d", projectGroup.Id))

		// 3. The tier of the environment this job belongs to, if set.
		if allowedForJob.Environment != nil {
			groups = append(groups, fmt.Sprintf("gitlab:group_env_tier:%d:%s", projectGroup.Id, allowedForJob.Environment.Tier))
		}
	}
	// 4. The project id.
	groups = append(groups, fmt.Sprintf("gitlab:project:%d", allowedForJob.Project.Id))
	// 5. The slug and tier of the environment this job belongs to, if set.
	if allowedForJob.Environment != nil {
		groups = append(groups,
			fmt.Sprintf("gitlab:project_env:%d:%s", allowedForJob.Project.Id, allowedForJob.Environment.Slug),
			fmt.Sprintf("gitlab:project_env_tier:%d:%s", allowedForJob.Project.Id, allowedForJob.Environment.Tier),
		)
	}
	return groups
}

func impCIJobExtra(allowedForJob *gapi.AllowedAgentsForJob, aa *gapi.AllowedAgent) []*kubernetes_api.ExtraKeyVal {
	extra := []*kubernetes_api.ExtraKeyVal{
		{
			Key: api.AgentIDKey,
			Val: []string{strconv.FormatInt(aa.Id, 10)}, // agent id
		},
		{
			Key: api.ConfigProjectIDKey,
			Val: []string{strconv.FormatInt(aa.ConfigProject.Id, 10)}, // agent's configuration project id
		},
		{
			Key: api.ProjectIDKey,
			Val: []string{strconv.FormatInt(allowedForJob.Project.Id, 10)}, // CI project id
		},
		{
			Key: api.CIPipelineIDKey,
			Val: []string{strconv.FormatInt(allowedForJob.Pipeline.Id, 10)}, // CI pipeline id
		},
		{
			Key: api.CIJobIDKey,
			Val: []string{strconv.FormatInt(allowedForJob.Job.Id, 10)}, // CI job id
		},
		{
			Key: api.UsernameKey,
			Val: []string{allowedForJob.User.Username}, // username of the user the CI job is running as
		},
	}
	if allowedForJob.Environment != nil {
		extra = append(extra,
			&kubernetes_api.ExtraKeyVal{
				Key: api.EnvironmentSlugKey,
				Val: []string{allowedForJob.Environment.Slug}, // slug of the environment, if set
			},
			&kubernetes_api.ExtraKeyVal{
				Key: api.EnvironmentTierKey,
				Val: []string{allowedForJob.Environment.Tier}, // tier of the environment, if set
			},
		)
	}
	return extra
}

func impUserGroups(user *gapi.AccessAsUserAuthorization) []string {
	groups := []string{"gitlab:user"}
	for _, accessCF := range user.Projects {
		for _, role := range accessCF.Roles {
			groups = append(groups, fmt.Sprintf("gitlab:project_role:%d:%s", accessCF.Id, role))
		}
	}
	for _, accessCF := range user.Groups {
		for _, role := range accessCF.Roles {
			groups = append(groups, fmt.Sprintf("gitlab:group_role:%d:%s", accessCF.Id, role))
		}
	}
	return groups
}

func impUserExtra(auth *gapi.AuthorizeProxyUserResponse, accessType string) []*kubernetes_api.ExtraKeyVal {
	extra := []*kubernetes_api.ExtraKeyVal{
		{
			Key: api.AgentIDKey,
			Val: []string{strconv.FormatInt(auth.Agent.Id, 10)},
		},
		{
			Key: api.UsernameKey,
			Val: []string{auth.User.Username},
		},
		{
			Key: api.AgentKeyPrefix + "/access_type",
			Val: []string{accessType},
		},
		{
			Key: api.ConfigProjectIDKey,
			Val: []string{strconv.FormatInt(auth.Agent.ConfigProject.Id, 10)},
		},
	}
	return extra
}
