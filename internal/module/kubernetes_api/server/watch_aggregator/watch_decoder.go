/*
Parts of this document are:

Copyright 2018 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package watch_aggregator //nolint:stylecheck

import (
	"io"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/ioz"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/runtime/serializer/json"
	"k8s.io/apimachinery/pkg/runtime/serializer/streaming"
	restclientwatch "k8s.io/client-go/rest/watch"
)

var serializer, streamingSerializer *json.Serializer

func init() {
	basicScheme := runtime.NewScheme()
	versionV1 := schema.GroupVersion{Version: "v1"}
	metav1.AddToGroupVersion(basicScheme, versionV1)
	serializer = json.NewSerializerWithOptions(json.DefaultMetaFactory, unstructuredCreater{basicScheme}, unstructuredTyper{basicScheme}, json.SerializerOptions{})
	streamingSerializer = json.NewSerializerWithOptions(json.DefaultMetaFactory, basicScheme, basicScheme, json.SerializerOptions{})
}

func newWatchDecoder(r io.ReadCloser) *restclientwatch.Decoder {
	frameReader := json.Framer.NewFrameReader(r)
	watchEventDecoder := streaming.NewDecoder(frameReader, streamingSerializer)
	return restclientwatch.NewDecoder(watchEventDecoder, serializer)
}

func newWatchErrorDecoder(r io.Reader) *watchErrorDecoder {
	return &watchErrorDecoder{r}
}

type watchErrorDecoder struct {
	r io.Reader
}

func (d *watchErrorDecoder) decode() (runtime.Object, error) {
	var err error
	var o runtime.Object
	err = ioz.ReadAllFunc(d.r, func(data []byte) error {
		o, err = runtime.Decode(serializer, data)
		return err
	})
	return o, err
}

type unstructuredCreater struct {
	nested runtime.ObjectCreater
}

func (c unstructuredCreater) New(kind schema.GroupVersionKind) (runtime.Object, error) {
	out, err := c.nested.New(kind)
	if err == nil {
		return out, nil
	}
	out = &unstructured.Unstructured{}
	out.GetObjectKind().SetGroupVersionKind(kind)
	return out, nil
}

type unstructuredTyper struct {
	nested runtime.ObjectTyper
}

func (t unstructuredTyper) ObjectKinds(obj runtime.Object) ([]schema.GroupVersionKind, bool, error) {
	kinds, unversioned, err := t.nested.ObjectKinds(obj)
	if err == nil {
		return kinds, unversioned, nil
	}
	if _, ok := obj.(runtime.Unstructured); ok && !obj.GetObjectKind().GroupVersionKind().Empty() {
		return []schema.GroupVersionKind{obj.GetObjectKind().GroupVersionKind()}, false, nil
	}
	return nil, false, err
}

func (t unstructuredTyper) Recognizes(gvk schema.GroupVersionKind) bool {
	return true
}
