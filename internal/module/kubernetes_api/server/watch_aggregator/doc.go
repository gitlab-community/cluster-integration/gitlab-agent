package watch_aggregator //nolint:stylecheck

//go:generate mockgen.sh -source "watch_aggregator.go" -destination "mock_watch_aggregator_for_test.go" -package "watch_aggregator"
//go:generate mockgen.sh -source "websocket.go" -destination "mock_websocket_for_test.go" -package "watch_aggregator"
