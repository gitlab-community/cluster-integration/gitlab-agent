package server

import (
	"testing"

	"github.com/bufbuild/protovalidate-go"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/kubernetes_api"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/jwttool"
)

const anyUserID = 84

func TestWebSocketToken_SuccessfulGenerateVerify(t *testing.T) {
	// GIVEN
	wt := setup(t, "test")
	testAgentID := int64(42)
	testImpConfig := &kubernetes_api.ImpersonationConfig{
		Username: "any-username",
		Groups:   []string{"any-group"},
		Uid:      "any-uid",
	}

	// WHEN
	token, err := wt.generate("any-url", testAgentID, anyUserID, testImpConfig)
	require.NoError(t, err)
	agentID, impConfig, err := wt.verify(token, "any-url")
	require.NoError(t, err)

	// THEN
	assert.Equal(t, testAgentID, agentID)
	assert.EqualExportedValues(t, testImpConfig, impConfig)
}

func TestWebSocketToken_InvalidEndpoint(t *testing.T) {
	// GIVEN
	wt := setup(t, "test")

	testImpConfig := &kubernetes_api.ImpersonationConfig{
		Username: "any-username",
		Groups:   []string{"any-group"},
		Uid:      "any-uid",
	}
	token, err := wt.generate("some-url", 42, anyUserID, testImpConfig)
	require.NoError(t, err)

	// WHEN
	_, _, err = wt.verify(token, "another-url")

	// THEN
	assert.EqualError(t, err, "token has invalid claims: token has invalid endpoint")
}

func TestWebSocketToken_InvalidSignature(t *testing.T) {
	// GIVEN
	wtGenerate := setup(t, "some-secret")
	wtVerify := setup(t, "another-secret")

	// WHEN
	token, err := wtGenerate.generate("any-url", 42, anyUserID, nil)
	require.NoError(t, err)
	_, _, err = wtVerify.verify(token, "any-url")

	// THEN
	assert.EqualError(t, err, "token signature is invalid: signature is invalid")
}

func TestWebSocketToken_InvalidEmptyToken(t *testing.T) {
	// GIVEN
	wt := setup(t, "any-secret")

	// WHEN
	_, _, err := wt.verify("", "any-url")

	// THEN
	assert.EqualError(t, err, "token is malformed: token contains an invalid number of segments")
}

func setup(t *testing.T, secret string) webSocketToken {
	v, err := protovalidate.New()
	require.NoError(t, err)
	wt := webSocketToken{
		validator:        v,
		jwtSigningMethod: jwttool.SigningMethodHS3_512,
		jwtSecret:        []byte(secret),
	}
	return wt
}
