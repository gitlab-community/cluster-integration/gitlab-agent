package server

import (
	"encoding/json"

	"github.com/golang-jwt/jwt/v5"
)

var (
	_ jwt.Claims          = (*WebSocketTokenClaims)(nil)
	_ json.Marshaler      = (*WebSocketTokenClaims)(nil)
	_ json.Unmarshaler    = (*WebSocketTokenClaims)(nil)
	_ jwt.ClaimsValidator = (*ValidatingWebSocketTokenClaims)(nil)
)
