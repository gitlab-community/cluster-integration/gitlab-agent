package managed_resources //nolint:stylecheck

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

func TestSplitNamespacedAndClusterScoped(t *testing.T) {
	objs := []*unstructured.Unstructured{
		{
			Object: map[string]any{
				"metadata": map[string]any{
					"namespace": "123",
				},
			},
		},
		{},
	}

	clusterScoped, namespaced := splitNamespacedAndClusterScoped(objs)

	assert.Empty(t, clusterScoped[0].GetNamespace())
	assert.NotEmpty(t, namespaced[0].GetNamespace())
}
