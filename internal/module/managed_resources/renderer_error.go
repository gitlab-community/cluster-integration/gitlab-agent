package managed_resources //nolint:stylecheck

import (
	"fmt"
)

type RendererErrorCode byte

const (
	_ RendererErrorCode = iota
	InvalidArgument
	Internal
)

var (
	_ error = (*RendererError)(nil)
)

func (c RendererErrorCode) String() string {
	switch c {
	case InvalidArgument:
		return "InvalidArgument"
	case Internal:
		return "Internal"
	default:
		return fmt.Sprintf("Unknown(%d)", byte(c))
	}
}

type RendererError struct {
	Msg  string
	Code RendererErrorCode
}

func (r *RendererError) Error() string {
	return fmt.Sprintf("RendererError(%s): msg = %s", r.Code, r.Msg)
}

func NewError(code RendererErrorCode, msg string) *RendererError {
	return &RendererError{
		Code: code,
		Msg:  msg,
	}
}

func NewErrorf(code RendererErrorCode, format string, a ...any) *RendererError {
	return NewError(
		code,
		fmt.Sprintf(format, a...),
	)
}
