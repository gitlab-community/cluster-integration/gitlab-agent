edition = "2023";

// If you make any changes make sure you run: make regenerate-proto

package gitlab.agent.managed_resources;

option go_package = "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/managed_resources";
option features.field_presence = IMPLICIT;

import "buf/validate/validate.proto";
import "pkg/constraint/constraint.proto";

// Information about the agent.
message Agent {
  int64 id = 1 [(buf.validate.field).int64.gt = 0];
  string name = 2 [(buf.validate.field).string.(constraint.is_agent_name) = true];
  string url = 3 [(buf.validate.field).string.uri = true];
}

message Pipeline {
  int64 id = 1 [(buf.validate.field).int64.gt = 0];
}

// Information about the environment project.
message Project {
  int64 id = 1 [(buf.validate.field).int64.gt = 0];
  string slug = 2 [(buf.validate.field).string.min_bytes = 1];
  string path = 3 [(buf.validate.field).string.min_bytes = 1];
  string url = 4 [(buf.validate.field).string.uri = true];
}

message Job {
  int64 id = 1 [(buf.validate.field).int64.gt = 0];
}

message User {
  int64 id = 1 [(buf.validate.field).int64.gt = 0];
  string username = 2 [(buf.validate.field).string.min_bytes = 1];
}

message Environment {
  int64 id = 1 [(buf.validate.field).int64.gt = 0];
  string name = 2 [(buf.validate.field).string.min_bytes = 1];
  string slug = 3 [(buf.validate.field).string.min_bytes = 1];
  // URL of the environment if provided by the user.
  string url = 4 [features.field_presence = EXPLICIT, (buf.validate.field).string.uri = true];
  // URL of the environment page.
  string page_url = 5 [(buf.validate.field).string.uri = true];
  string tier = 6 [(buf.validate.field).string.min_bytes = 1];
}

message TemplatingInfo {
  Agent agent = 1 [(buf.validate.field).required = true];
  Environment environment = 2 [(buf.validate.field).required = true];
  Project project = 4 [(buf.validate.field).required = true];
  Pipeline pipeline = 5 [(buf.validate.field).required = true];
  Job job = 6 [(buf.validate.field).required = true];
  User user = 7 [(buf.validate.field).required = true];
}

message EnvironmentInfo {
  int64 environment_id = 1 [json_name = "environment_id", (buf.validate.field).int64.gt = 0];
  string environment_name = 2 [json_name = "environment_name", (buf.validate.field).string.min_bytes = 1];
  string environment_slug = 3 [json_name = "environment_slug", (buf.validate.field).string.min_bytes = 1];
  string environment_url = 4 [json_name = "environment_url", features.field_presence = EXPLICIT, (buf.validate.field).string.uri = true];
  string environment_page_url = 5 [json_name = "environment_page_url", (buf.validate.field).string.uri = true];
  string environment_tier = 6 [json_name = "environment_tier", (buf.validate.field).string.min_bytes = 1];
  int64 agent_id = 7 [json_name = "agent_id", (buf.validate.field).int64.gt = 0];
  string agent_name = 8 [json_name = "agent_name", (buf.validate.field).string.(constraint.is_agent_name) = true];
  string agent_url = 9 [json_name = "agent_url", (buf.validate.field).string.uri = true];
  int64 project_id = 10 [json_name = "project_id", (buf.validate.field).int64.gt = 0];
  string project_slug = 11 [json_name = "project_slug", (buf.validate.field).string.min_bytes = 1];
  string project_path = 12 [json_name = "project_path", (buf.validate.field).string.min_bytes = 1];
  string project_url = 13 [json_name = "project_url", (buf.validate.field).string.uri = true];
  string template_name = 14 [json_name = "template_name", (buf.validate.field).string.min_bytes = 1];
}

message EnvironmentTemplate {
  string name = 1 [json_name = "name", (buf.validate.field).string.min_bytes = 1];
  bytes data = 2 [json_name = "data"];
}

message RenderedEnvironmentTemplate {
  string name = 1 [json_name = "name", (buf.validate.field).string.min_bytes = 1];
  bytes data = 2 [json_name = "data"];
}
