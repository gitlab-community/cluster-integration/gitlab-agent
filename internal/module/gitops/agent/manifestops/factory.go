package manifestops

import (
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modagent"
)

type Factory struct {
}

func (f *Factory) IsProducingLeaderModules() bool {
	return false
}

func (f *Factory) New(config *modagent.Config) (modagent.Module, error) {
	return &module{log: config.Log}, nil
}

func (f *Factory) Name() string {
	return "gitops-manifest"
}
