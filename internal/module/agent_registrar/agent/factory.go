package agent

import (
	"fmt"
	"time"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_registrar"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_registrar/rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modagent"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/retry"
)

const (
	registerInitBackoff   = 10 * time.Second
	registerMaxBackoff    = 5 * time.Minute
	registerResetDuration = 10 * time.Minute
	registerBackoffFactor = 2.0
	registerJitter        = 5.0
)

type Factory struct {
}

func (f *Factory) IsProducingLeaderModules() bool {
	return false
}

func (f *Factory) Name() string {
	return agent_registrar.ModuleName
}

func (f *Factory) New(config *modagent.Config) (modagent.Module, error) {
	kubeClientset, err := config.K8sUtilFactory.KubernetesClientSet()
	if err != nil {
		return nil, fmt.Errorf("could not create kubernetes clientset: %w", err)
	}

	config.RegisterKASAPI(&rpc.AgentRegistrar_ServiceDesc)
	m := &module{
		Log:        config.Log,
		AgentMeta:  config.AgentMeta,
		InstanceID: config.InstanceID,
		PollConfig: retry.NewPollConfigFactory(agent_registrar.RegisterAttemptInterval, retry.NewExponentialBackoffFactory(
			registerInitBackoff,
			registerMaxBackoff,
			registerResetDuration,
			registerBackoffFactor,
			registerJitter,
		)),
		Client:      rpc.NewAgentRegistrarClient(config.KASConn),
		KubeVersion: kubeClientset.Discovery(),
	}
	return m, nil
}
