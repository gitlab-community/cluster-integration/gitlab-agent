package agent

import (
	"context"

	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

type mockInformer struct {
	Resources   map[string]map[string]*unstructured.Unstructured
	AddResource func(any)
}

func newMockInformer(resourceAdder func(any)) *mockInformer {
	return &mockInformer{
		Resources:   make(map[string]map[string]*unstructured.Unstructured),
		AddResource: resourceAdder,
	}
}

func (i *mockInformer) Start(ctx context.Context) error {
	return nil
}

func (i *mockInformer) ListAll() []*unstructured.Unstructured {
	result := make([]*unstructured.Unstructured, 0, len(i.Resources))
	for _, namespaces := range i.Resources {
		for _, resource := range namespaces {
			result = append(result, resource)
		}

	}
	return result
}

func (i *mockInformer) Get(namespace, name string) (*unstructured.Unstructured, error) {
	objects, exit := i.Resources[namespace]
	if !exit {
		return nil, k8serrors.NewNotFound(namespaceGVR.GroupResource(), namespace)
	}
	obj, exist := objects[name]
	if exist {
		return obj, nil
	}

	return nil, k8serrors.NewNotFound(secretGVR.GroupResource(), name)

}

func (i *mockInformer) Stop() {
	// do nothing
}
