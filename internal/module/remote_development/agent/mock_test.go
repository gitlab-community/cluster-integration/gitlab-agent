package agent

// TODO: use mockgen and refactor existing tests see https://gitlab.com/gitlab-org/gitlab/-/issues/498218

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/require"
	corev1 "k8s.io/api/core/v1"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/kubectl/pkg/scheme"
)

type MockClient struct {
	NamespaceStore   map[string]*unstructured.Unstructured
	SecretStore      map[string]map[string]*unstructured.Unstructured
	ApplyRecorder    string
	MockErrorApplier error
	MockErrorClient  error
}

func NewMockClient() *MockClient {
	return &MockClient{
		NamespaceStore: map[string]*unstructured.Unstructured{},
		SecretStore:    map[string]map[string]*unstructured.Unstructured{},
	}
}

func (m *MockClient) Get(ctx context.Context, gvr schema.GroupVersionResource, namespace, name string) (*unstructured.Unstructured, error) {
	if gvr.Group == "" && gvr.Version == "v1" && gvr.Resource == "secrets" {
		secretNamespace, exist := m.SecretStore[namespace]
		if !exist {
			return nil, k8serrors.NewNotFound(namespaceGVR.GroupResource(), namespace)
		}
		obj, exist := secretNamespace[name]
		if !exist {
			return nil, k8serrors.NewNotFound(secretGVR.GroupResource(), name)
		}
		return obj, nil
	}

	if gvr.Group == "" && gvr.Version == "v1" && gvr.Resource == "namespaces" {
		obj, exist := m.NamespaceStore[name]
		if !exist {
			return nil, k8serrors.NewNotFound(secretGVR.GroupResource(), name)
		} else {
			return obj, nil
		}
	}

	return nil, errors.New("resource schema not supported")
}

func (m *MockClient) UpdateOrCreate(ctx context.Context, gvr schema.GroupVersionResource, namespace string, obj runtime.Object, force bool) (*unstructured.Unstructured, error) {
	u := &unstructured.Unstructured{}
	if m.MockErrorClient != nil {
		return nil, m.MockErrorClient
	}
	if gvr.Group == "" && gvr.Version == "v1" && gvr.Resource == "secrets" {
		err := scheme.Scheme.Convert(obj, u, nil)
		if err != nil {
			return nil, err
		}
		_, exist := m.SecretStore[namespace]
		if !exist {
			m.SecretStore[namespace] = make(map[string]*unstructured.Unstructured)
		}
		m.SecretStore[namespace][u.GetName()] = u
		return u, nil
	}
	if gvr.Group == "" && gvr.Version == "v1" && gvr.Resource == "namespaces" {
		err := scheme.Scheme.Convert(obj, u, nil)
		if err != nil {
			return nil, err
		}
		m.NamespaceStore[namespace] = u
		return u, nil
	}

	return nil, errors.New("resource schema not supported")
}

func (m *MockClient) Delete(ctx context.Context, gvr schema.GroupVersionResource, namespace, name string) error {
	if gvr.Group == "" && gvr.Version == "v1" && gvr.Resource == "secrets" {
		namespace, exist := m.SecretStore[namespace]
		if !exist {
			return nil
		}
		delete(namespace, name)
	}

	if gvr.Group == "" && gvr.Version == "v1" && gvr.Resource == "namespaces" {
		delete(m.NamespaceStore, namespace)
	}
	return nil
}

func (m *MockClient) Apply(_ context.Context, config string) <-chan error {
	m.ApplyRecorder = config

	if m.MockErrorApplier != nil {
		errorCh := make(chan error)
		go func() {
			defer close(errorCh)
			errorCh <- m.MockErrorApplier
			m.MockErrorApplier = nil
		}()

		return errorCh
	}

	return nil
}

func generateImagePullSecret(name string, namespace string, labels map[string]string, data map[string][]byte) *corev1.Secret {
	return &corev1.Secret{
		TypeMeta: v1.TypeMeta{
			Kind:       "Secret",
			APIVersion: "v1",
		},
		ObjectMeta: v1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
			Labels:    labels,
		},
		Data: data,
		Type: "kubernetes.io/dockercfg",
	}
}

func toUnstructuredSecret(t *testing.T, secret *corev1.Secret) *unstructured.Unstructured {
	object, err := runtime.DefaultUnstructuredConverter.ToUnstructured(secret)
	require.NoError(t, err)

	return &unstructured.Unstructured{
		Object: object,
	}
}

func fromUnstructuredSecret(t *testing.T, unstructuredSecret *unstructured.Unstructured) *corev1.Secret {
	var secret *corev1.Secret
	err := runtime.DefaultUnstructuredConverter.FromUnstructured(unstructuredSecret.Object, &secret)
	require.NoError(t, err)
	return secret
}
