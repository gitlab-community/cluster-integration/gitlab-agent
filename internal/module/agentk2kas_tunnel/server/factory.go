package server

import (
	"time"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agentk2kas_tunnel"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agentk2kas_tunnel/router"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/retry"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tunnel/rpc"
)

const (
	getAgentInfoInitBackoff   = 20 * time.Millisecond
	getAgentInfoMaxBackoff    = 5 * time.Minute
	getAgentInfoResetDuration = 10 * time.Minute
	getAgentInfoBackoffFactor = 2.0
	getAgentInfoJitter        = 5.0
)

type Factory struct {
	TunnelHandler router.Handler
}

func (f *Factory) New(config *modserver.Config) (modserver.Module, error) {
	rpc.RegisterReverseTunnelServer(config.AgentServer, &server{
		tunnelHandler: f.TunnelHandler,
		getAgentInfoPollConfig: retry.NewPollConfigFactory(0, retry.NewExponentialBackoffFactory(
			getAgentInfoInitBackoff,
			getAgentInfoMaxBackoff,
			getAgentInfoResetDuration,
			getAgentInfoBackoffFactor,
			getAgentInfoJitter,
		)),
	})
	return nil, nil
}

func (f *Factory) Name() string {
	return agentk2kas_tunnel.ModuleName
}
