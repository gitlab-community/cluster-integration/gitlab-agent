package router

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/mock_modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/testhelpers"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/testlogger"
	metricnoop "go.opentelemetry.io/otel/metric/noop"
	"go.uber.org/mock/gomock"
	"k8s.io/apimachinery/pkg/util/wait"
)

func TestAsyncTrackerBatching1(t *testing.T) {
	ctrl := gomock.NewController(t)
	api := mock_modshared.NewMockAPI(ctrl)
	mp := metricnoop.NewMeterProvider().Meter("test")
	tracker := NewMockTracker(ctrl)
	rb := NewMockRegistrationBuilder(ctrl)

	lots := make([]int64, maxBatchSize*2+3)
	for i := range lots {
		lots[i] = int64(i)
	}

	gomock.InOrder(
		tracker.EXPECT().
			RegistrationBuilder().
			Return(rb),
		rb.EXPECT().
			Refresh(time.Minute, lots[:maxBatchSize]),
		rb.EXPECT().
			Do(gomock.Any()),
		rb.EXPECT().
			Refresh(time.Minute, lots[maxBatchSize:maxBatchSize+maxBatchSize]),
		rb.EXPECT().
			Do(gomock.Any()),
		rb.EXPECT().
			Refresh(time.Minute, lots[maxBatchSize+maxBatchSize:]),
		rb.EXPECT().
			Do(gomock.Any()),
	)

	a, err := newAsyncTracker(testlogger.New(t), api, mp, tracker, time.Minute)
	require.NoError(t, err)

	var wg wait.Group
	defer wg.Wait()
	defer a.done()
	wg.Start(a.run)

	a.refresh(lots)
}

func TestAsyncTrackerBatching2(t *testing.T) {
	ctrl := gomock.NewController(t)
	api := mock_modshared.NewMockAPI(ctrl)
	mp := metricnoop.NewMeterProvider().Meter("test")
	tracker := NewMockTracker(ctrl)
	rb := NewMockRegistrationBuilder(ctrl)

	lots := make([]int64, maxBatchSize)
	for i := range lots {
		lots[i] = int64(i)
	}
	stop := make(chan struct{})
	gomock.InOrder(
		tracker.EXPECT().
			RegistrationBuilder().
			Return(rb),
		rb.EXPECT().
			Register(time.Minute, testhelpers.AgentID).
			Do(func(ttl time.Duration, agentIDs ...int64) {
				<-stop // wait for the refresh() to put task into the queue so that we batch it with this registration.
			}),
		rb.EXPECT().
			Refresh(time.Minute, lots[:maxBatchSize-1]),
		rb.EXPECT().
			Do(gomock.Any()),
		rb.EXPECT().
			Refresh(time.Minute, lots[maxBatchSize-1:]),
		rb.EXPECT().
			Do(gomock.Any()),
	)

	a, err := newAsyncTracker(testlogger.New(t), api, mp, tracker, time.Minute)
	require.NoError(t, err)

	var wg wait.Group
	defer wg.Wait()
	defer a.done()
	wg.Start(a.run)

	a.register(testhelpers.AgentID)
	a.refresh(lots)
	close(stop)
}
