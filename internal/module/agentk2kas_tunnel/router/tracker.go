package router

import (
	"context"
	"errors"
	"fmt"
	"net/netip"
	"time"
	"unsafe"

	"github.com/bufbuild/protovalidate-go"
	"github.com/redis/rueidis"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/nettool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/redistool"
	otelmetric "go.opentelemetry.io/otel/metric"
	"google.golang.org/protobuf/proto"
	"k8s.io/apimachinery/pkg/util/sets"
	"k8s.io/utils/clock"
)

const (
	tunnelsByAgentIDHashName = "tunnels_by_agent_id"
)

type Querier interface {
	// KASURLsByAgentID returns the list of kas URLs for a particular agent id.
	// A partial list may be returned together with an error.
	// Safe for concurrent use.
	KASURLsByAgentID(ctx context.Context, agentID int64) ([]nettool.MultiURL, error)
}

// RegistrationBuilder allows batching tunnel (un)registrations.
// Can be reused after Do is called.
type RegistrationBuilder interface {
	// Register registers tunnels for the given agent ids with the tracker.
	Register(ttl time.Duration, agentIDs ...int64)
	// Unregister unregisters tunnels for the given agent ids with the tracker.
	Unregister(agentIDs ...int64)
	// Refresh refreshes registered tunnels in the underlying storage.
	Refresh(ttl time.Duration, agentIDs ...int64)
	// Do executes the enqueued operations.
	Do(context.Context) error
}

// Registerer allows to register and unregister tunnels.
// Caller is responsible for periodically calling GC() and Refresh().
// Not safe for concurrent use.
type Registerer interface {
	RegistrationBuilder() RegistrationBuilder
	// GC deletes expired tunnels from the underlying storage.
	GC(ctx context.Context, agentIDs []int64) (int /* keysDeleted */, error)
}

type Tracker interface {
	Registerer
	Querier
}

type RedisTracker struct {
	validator        protovalidate.Validator
	ownPrivateAPIURL string //
	clock            clock.PassiveClock
	tunnelsByAgentID redistool.ExpiringHashAPI[int64, string] // agentID -> kas multi URL proto -> nil
}

func NewRedisTracker(client rueidis.Client, validator protovalidate.Validator, agentKeyPrefix string, ownPrivateAPIURL nettool.MultiURL, m otelmetric.Meter) (*RedisTracker, error) {
	tunnelsByAgentID, err := redistool.NewRedisExpiringHashAPI[int64, string](tunnelsByAgentIDHashName, client, tunnelsByAgentIDHashKey(agentKeyPrefix), strToStr, m)
	if err != nil {
		return nil, err
	}

	ownPrivateAPIURLProto, err := proto.Marshal(fromMultiURL(ownPrivateAPIURL))
	if err != nil {
		return nil, err
	}

	return &RedisTracker{
		validator:        validator,
		ownPrivateAPIURL: string(ownPrivateAPIURLProto),
		clock:            clock.RealClock{},
		tunnelsByAgentID: tunnelsByAgentID,
	}, nil
}

func (t *RedisTracker) RegistrationBuilder() RegistrationBuilder {
	return &redisRegistrationBuilder{
		ownPrivateAPIURL: t.ownPrivateAPIURL,
		clock:            t.clock,
		iob:              t.tunnelsByAgentID.IOBuilder(),
	}
}

func (t *RedisTracker) KASURLsByAgentID(ctx context.Context, agentID int64) ([]nettool.MultiURL, error) {
	urls := make(sets.Set[nettool.MultiURL])
	var errs []error
	err := t.tunnelsByAgentID.Scan(ctx, agentID, func(rawHashKey string, value []byte, err error) (bool, error) {
		if err != nil {
			errs = append(errs, err)
			return false, nil
		}
		var mu MultiURL
		// Avoid creating a temporary copy
		rawHashKeyBytes := unsafe.Slice(unsafe.StringData(rawHashKey), len(rawHashKey)) //nolint: gosec
		err = proto.Unmarshal(rawHashKeyBytes, &mu)
		if err != nil {
			errs = append(errs, err)
			return false, nil
		}
		err = t.validator.Validate(&mu) // validate just in case. We don't fully trust the stuff from Redis.
		if err != nil {
			errs = append(errs, err)
			return false, nil
		}

		var x nettool.MultiURL
		switch {
		case mu.Host != "":
			x = nettool.NewMultiURLForHost(mu.Scheme, mu.Host, mu.TlsHost, uint16(mu.Port)) //nolint:gosec
		case len(mu.Ip) > 0:
			addrs := make([]netip.Addr, 0, len(mu.Ip))
			for _, ip := range mu.Ip {
				netIP, ok := netip.AddrFromSlice(ip)
				if !ok {
					errs = append(errs, fmt.Errorf("invalid IP: 0x%x", ip))
					return false, nil
				}
				addrs = append(addrs, netIP.Unmap()) // unmap just in case. We don't fully trust the stuff from Redis.
			}
			x = nettool.NewMultiURLForAddresses(mu.Scheme, mu.TlsHost, uint16(mu.Port), addrs) //nolint:gosec
		default:
			// this shouldn't happen - we validate the proto above
			errs = append(errs, errors.New("neither host or IP provided"))
			return false, nil
		}
		urls.Insert(x)
		return false, nil
	})
	if err != nil {
		errs = append(errs, err)
	}
	return urls.UnsortedList(), errors.Join(errs...)
}

func (t *RedisTracker) GC(ctx context.Context, agentIDs []int64) (int /* keysDeleted */, error) {
	return t.tunnelsByAgentID.GCFor(agentIDs, true)(ctx)
}

type redisRegistrationBuilder struct {
	ownPrivateAPIURL string
	clock            clock.PassiveClock
	iob              redistool.IOBuilder[int64, string]
}

func (b *redisRegistrationBuilder) Register(ttl time.Duration, agentIDs ...int64) {
	exp := b.clock.Now().Add(ttl)
	b.iob.Set(agentIDs, ttl, kv(b.ownPrivateAPIURL, exp))
}

func (b *redisRegistrationBuilder) Unregister(agentIDs ...int64) {
	b.iob.Unset(agentIDs, b.ownPrivateAPIURL)
}

func (b *redisRegistrationBuilder) Refresh(ttl time.Duration, agentIDs ...int64) {
	// Refresh is the same as registration right now, but this might change in the future.
	b.Register(ttl, agentIDs...)
}

func (b *redisRegistrationBuilder) Do(ctx context.Context) error {
	return b.iob.Do(ctx)
}

func kv(key string, expiresAt time.Time) redistool.BuilderKV[string] {
	return redistool.BuilderKV[string]{
		HashKey: key,
		Value: &redistool.ExpiringValue{
			ExpiresAt: expiresAt.Unix(),
			Value:     nil, // nothing to store.
		},
	}
}

// tunnelsByAgentIDHashKey returns a key for agentID -> (kas multi URL proto -> nil).
func tunnelsByAgentIDHashKey(agentKeyPrefix string) redistool.KeyToRedisKey[int64] {
	prefix := agentKeyPrefix + ":kas_by_agent_id3:"
	return func(agentID int64) string {
		return redistool.PrefixedInt64Key(prefix, agentID)
	}
}

func strToStr(key string) string {
	return key
}

func fromMultiURL(u nettool.MultiURL) *MultiURL {
	addrs := u.Addresses()
	ips := make([][]byte, 0, len(addrs))
	for _, addr := range addrs {
		ips = append(ips, addr.AsSlice())
	}
	return &MultiURL{
		Scheme:  u.Scheme(),
		Ip:      ips,
		Host:    u.Host(),
		TlsHost: u.TLSHost(),
		Port:    uint32(u.Port()),
	}
}
