package agent_configuration //nolint:stylecheck

import (
	"fmt"
)

const (
	Directory           = ".gitlab/agents"
	FileName            = "config.yaml"
	ModuleName          = "agent_configuration"
	RefNotFoundCommitID = "ref not found"
)

// DirectoryForAgent returns the full path for the agent's configuration directory.
// name is the agent name. Must be a valid name - this function does not perform validation.
func DirectoryForAgent(name string) string {
	return fmt.Sprintf("%s/%s", Directory, name)
}

// FileForAgent returns the full path for the agent's configuration file.
// name is the agent name. Must be a valid name - this function does not perform validation.
func FileForAgent(name string) string {
	return fmt.Sprintf("%s/%s", DirectoryForAgent(name), FileName)
}
