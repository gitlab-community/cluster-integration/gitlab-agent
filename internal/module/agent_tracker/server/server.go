package server

import (
	"context"
	"log/slog"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_tracker"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_tracker/rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/version"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type server struct {
	rpc.UnsafeAgentTrackerServer
	log                *slog.Logger
	agentQuerier       agent_tracker.Querier
	serverVersion      version.Version
	gitlabReleasesList []string
}

func (s *server) GetConnectedAgentsByProjectIDs(ctx context.Context, req *rpc.GetConnectedAgentsByProjectIDsRequest) (*rpc.GetConnectedAgentsByProjectIDsResponse, error) {
	rpcAPI := modshared.RPCAPIFromContext(ctx)
	log := rpcAPI.Log()
	var infos agent_tracker.ConnectedAgentInfoCollector
	for _, projectID := range req.ProjectIds {
		err := s.agentQuerier.GetConnectionsByProjectID(ctx, projectID, infos.Collect)
		if err != nil {
			rpcAPI.HandleProcessingError(log, "GetConnectionsByProjectID() failed", err)
			return nil, status.Error(codes.Unavailable, "GetConnectionsByProjectID() failed")
		}
	}

	return &rpc.GetConnectedAgentsByProjectIDsResponse{
		Agents: s.constructFromAgentTrackerConnectedAgentInfos(infos),
	}, nil
}

func (s *server) GetConnectedAgentsByAgentIDs(ctx context.Context, req *rpc.GetConnectedAgentsByAgentIDsRequest) (*rpc.GetConnectedAgentsByAgentIDsResponse, error) {
	rpcAPI := modshared.RPCAPIFromContext(ctx)
	log := rpcAPI.Log()
	var infos agent_tracker.ConnectedAgentInfoCollector
	for _, agentID := range req.AgentIds {
		err := s.agentQuerier.GetConnectionsByAgentID(ctx, agentID, infos.Collect)
		if err != nil {
			rpcAPI.HandleProcessingError(log, "GetConnectionsByAgentID() failed", err)
			return nil, status.Error(codes.Unavailable, "GetConnectionsByAgentID() failed")
		}
	}
	return &rpc.GetConnectedAgentsByAgentIDsResponse{
		Agents: s.constructFromAgentTrackerConnectedAgentInfos(infos),
	}, nil
}

func (s *server) CountAgentsByAgentVersions(ctx context.Context, _ *rpc.CountAgentsByAgentVersionsRequest) (*rpc.CountAgentsByAgentVersionsResponse, error) {
	rpcAPI := modshared.RPCAPIFromContext(ctx)
	log := rpcAPI.Log()

	counts, err := s.agentQuerier.CountAgentsByAgentVersions(ctx)
	if err != nil {
		rpcAPI.HandleProcessingError(log, "CountAgentsByAgentVersions() failed", err)
		return nil, status.Error(codes.Unavailable, "CountAgentsByAgentVersions() failed")
	}

	return &rpc.CountAgentsByAgentVersionsResponse{
		AgentVersions: counts,
	}, nil
}

func (s *server) constructFromAgentTrackerConnectedAgentInfos(infos []*agent_tracker.ConnectedAgentInfo) []*rpc.ConnectedAgent {
	cas := make([]*rpc.ConnectedAgent, len(infos))
	for i, info := range infos {
		cas[i] = &rpc.ConnectedAgent{
			AgentMeta:    info.AgentMeta,
			ConnectedAt:  info.ConnectedAt,
			ConnectionId: info.ConnectionId,
			AgentId:      info.AgentId,
			ProjectId:    info.ProjectId,
			Warnings:     s.getAgentsWarnings(info),
		}
	}
	return cas
}

func (s *server) getAgentsWarnings(info *agent_tracker.ConnectedAgentInfo) []*rpc.AgentWarning {
	// Currently only a warning for the agent version is implemented.
	if info.AgentMeta == nil {
		return nil
	}

	agentVersion, err := version.NewVersion(info.AgentMeta.Version)
	if err != nil {
		s.log.Debug("Version received from agentk is not a valid semver", logz.Error(err), slog.String("version", info.AgentMeta.Version))
		return nil
	}

	versionWarningType, versionWarning := version.WarningIfOutdatedAgent(s.serverVersion, agentVersion, s.gitlabReleasesList)
	switch versionWarningType { //nolint:exhaustive
	case version.AgentVersionNoWarning:
		return nil
	default:
		return []*rpc.AgentWarning{
			{
				Warning: &rpc.AgentWarning_Version{
					Version: &rpc.AgentWarningVersion{
						Type:    string(versionWarningType),
						Message: versionWarning,
					},
				},
			},
		}
	}
}
