package rpc

import (
	"google.golang.org/grpc/metadata"
)

func (x *RequestInfo) Metadata() metadata.MD {
	return MetadataKVToMeta(x.Meta)
}

func (x *Header) Metadata() metadata.MD {
	return MetadataKVToMeta(x.Meta)
}

func (x *Trailer) Metadata() metadata.MD {
	return MetadataKVToMeta(x.Meta)
}

func (x *GatewayResponse_Header) Metadata() metadata.MD {
	return MetadataKVToMeta(x.Meta)
}

func (x *GatewayResponse_Trailer) Metadata() metadata.MD {
	return MetadataKVToMeta(x.Meta)
}
