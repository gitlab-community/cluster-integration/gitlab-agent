## 17.9.1 (2025-02-26)

No changes.

## 17.9.0 (2025-02-19)

### Fixed (3 changes)

- [Handle Request#Body == nil for outbound requests](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/d227bb2a25d014dcebe17351fa349304c3684d66) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/2216))
- [Support name resolution when detecting own private API URL](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/934f0a87dad80c5a65202c9093c1fe83bf45f1d3) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/2181))
- [Allow loopback and link-local addresses, log a warning](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/aa51e979d8c9a40d24c8f0a1f510842624e147f7) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/2181))

### Changed (1 change)

- [Use grpcs scheme by default when TLS is configured on private API listener](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/b576af47e4eccf285d595aa74cf92f9c0b3be582) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/2207))

## 17.8.4 (2025-02-26)

No changes.

## 17.8.3 (2025-02-21)

No changes.

## 17.8.2 (2025-02-11)

No changes.

## 17.8.1 (2025-01-22)

### Fixed (2 changes)

- [Support name resolution when detecting own private API URL](https://gitlab.com/gitlab-org/security/cluster-integration/gitlab-agent/-/commit/ef3943768c8aa840dacd7c924d6b7dbd7a4e4438)
- [Allow loopback and link-local addresses, log a warning](https://gitlab.com/gitlab-org/security/cluster-integration/gitlab-agent/-/commit/f0c799f760183951fc0d6b94d62a27f4de385095)

## 17.8.0 (2025-01-15)

### Performance (1 change)

- [Bump max number of tunnel connections](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/17e1d0b5decd7a7f53e73e964704c4219debb798) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/2135))

## 17.7.6 (2025-02-26)

No changes.

## 17.7.5 (2025-02-21)

No changes.

## 17.7.4 (2025-02-11)

No changes.

## 17.7.3 (2025-01-22)

No changes.

## 17.7.2 (2025-01-14)

No changes.

## 17.7.1 (2025-01-08)

No changes.

## 17.7.0 (2024-12-18)

### Added (3 changes)

- [Add accepted and closed connection counters](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/54b3ddb0acd93a2d28107fb406387dc002c9b66a) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/2030))
- [kas and agentk integration test](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/b75166265322fb31d3ea547f5d97c1f7fea9c454) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1575))
- [Implement WebSocket Token Request Endpoint and Auth](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/a547df5b4115a2bd1319ab2341954f67266b5f4b) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1967))

### Changed (5 changes)

- [Set timeout on sending agent config to GitLab](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/d157a325db345cd6d304531f6ef78d7bdd71721f) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/2033))
- [Send agent config to GitLab synchronously](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/efa3ed080e9c9d4223c19e3d6ed65a1f07efa941) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/2033))
- [Enable literal path spec option](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/1abb28adf3101292bd6b451919f98fd1c8c708f8) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/2033))
- [Bump Debian images](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/2147da09c1cd15285eb7eec45b4ff3846b801605) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/2032))
- [Reduce allocations in LimiterWrapper](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/e22e30ed351d20fa92677affdc0df86530626d2b) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/2030))

### Security (1 change)

- [Do not forward NEL headers but always send header to clear existing policies](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/be8fba1ddc734c7af6d0e0e70b0685607e4e42c5)

## 17.6.5 (2025-02-11)

No changes.

## 17.6.4 (2025-01-22)

No changes.

## 17.6.3 (2025-01-08)

No changes.

## 17.6.2 (2024-12-10)

No changes.

## 17.6.1 (2024-11-26)

No changes.

## 17.6.0 (2024-11-20)

### Fixed (2 changes)

- [Support passing binary HTTP query keys and values](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/de9232bec432df032f960d8594be2901d5cfebe8) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1942))
- [Support passing empty and binary HTTP header values](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/12576e8634774d2ddc2f8ba9b9efab8ec3f53bb2) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1932))

## 17.5.5 (2025-01-08)

No changes.

## 17.5.4 (2024-12-10)

No changes.

## 17.5.3 (2024-11-26)

No changes.

## 17.5.2 (2024-11-12)

### Fixed (1 change)

- [Support passing binary HTTP query keys and values](https://gitlab.com/gitlab-org/security/cluster-integration/gitlab-agent/-/commit/a0e7dca43a894fd17703768af973f500b826adee)

## 17.5.1 (2024-10-22)

No changes.

## 17.5.0 (2024-10-16)

### Added (6 changes)

- [Per-listener connection sets to track dropped connections](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/be99a8d66ee2d2c6749ec6429b6c7e817ff4b390) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1882))
- [Validate agent meta using validator](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/9638f77a6e7a1fd79febee5f20fa0ef3d6296593) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1849))
- [Log program startup errors using the logger](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/8967a64f5a21dd4ca5bffe61e362efb07ed9f290) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1849))
- [Send OTEL errors to Sentry](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/9deadd3041a6271933a683067885196418f7c04e) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1849))
- [Listener accepted/closed metrics](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/c50b8c9f6c853a999bcf6e6ddc940ea5e4f3bd1e) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1835))
- [Enabled HTTP/2 for Kubernetes proxy when in TLS mode](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/17604f7b04f4fc9e07efe4e549dba8ba93a1019d) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1835))

### Fixed (5 changes)

- [Unblock outbound client if hijacked connection breaks](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/2ae0678789b53cba2337d7d63a8da151e34fc70a) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1840))
- [Use a single root context for all watches](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/81c41201e3de00aa67af32e6846a82e27104d4e0) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1839))
- [Track and shut down hijacked connections in Kubernetes proxy](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/f6f85b56f868c7304bf48b6902259ac588348919) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1824))
- [Close pooled connection to self](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/ac083f02b2291677f38d5e3caf4ef70eb2c9a27f) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1820))
- [Close connection pool only once](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/0ed6e8f90107f894486801dd2b03f2f786668c1a) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1820))

### Changed (1 change)

- [Simplify error handling in proxy code](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/4c113a4c7ea15cda7e1a2331b9ef96afb0dcb2ec) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1840))

### Performance (5 changes)

- [Preallocate counter add options](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/50a0d035486aa33fd97c134b777c30808260d574) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1882))
- [Manually read all from the reader and reuse buffer](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/bb985f21f6626c52e171c8cacdf33ba81b201e74) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1836))
- [Replace io.ReadAll() with a custom version that pools memory](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/34ef23ee2df9a0edd48a7d43c37eaaf2718c2a5c) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1833))
- [Use gRPC's buffer pooling instead of our own](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/b96048d57eb4086683bb9adb4ffe3c147ca912a1) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1799))
- [Use gRPC's buffer pooling in codecs](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/d4d275e68deef990265d0d062e8175745f64fc3c) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1799))

## 17.4.6 (2024-12-10)

No changes.

## 17.4.5 (2024-11-26)

No changes.

## 17.4.4 (2024-11-12)

No changes.

## 17.4.3 (2024-10-22)

No changes.

## 17.4.2 (2024-10-09)

No changes.

## 17.4.1 (2024-09-24)

No changes.

## 17.4.0 (2024-09-18)

### Added (9 changes)

- [Granular on/off tracing for HTTP servers](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/818cad28f242fadd92cfd835569d891e0762f14c) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1809))
- [Granular on/off tracing for HTTP clients](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/985f6862e97666861ef7403ade8d759bfa33abc5) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1809))
- [Granular on/off tracing for the Redis client](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/dd1baff80ed51b376e11a6fa6d5b05f6247c1942) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1809))
- [Granular on/off tracing for gRPC clients](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/16adf474aa79a55b76c6eae8002d75beb8420679) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1809))
- [Granular on/off tracing for gRPC servers](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/9f4f682268bda395f94d26bf1d14c05a17254772) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1809))
- [More granular tracing configuration](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/1cee95744638a1b2994ea39dce988bae86cdff2b) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1809))
- [Add Deployment to list of workloads to scan](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/edf84ebd75253b4e59d24214a532568f80d8a515) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1772))
- [Metrics for async tunnel tracker](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/6fcc6b278520b3102f42b2e9d0ad725469be6d47) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1740))
- [git_ref attribute to track deployments](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/8545b6ddc530119473bf167d80293954762aa855) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1710))

### Fixed (9 changes)

- [Use client handler in AutoFlow module](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/6407e28cf3d6d3e1f7d901481d7e2d2a8a66da41) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1809))
- [Validate Ed25519 key size](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/0ffc27eb37cd3832f9c230511f7ec058bca9c68e) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1805))
- [Make WorkerManager wait for all stopped workers](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/48f7ada7ef65b0483905b7ad9f3cf6fe7568a180) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1801))
- [Fix receptive agents module shutdown](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/14141611f7569a2589599523867f3d509317dbc1) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1789))
- [Disable receptive agents](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/220ade0a9398c761f4449d8151bcc7c81222d99b) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1782))
- [Pick up otelhttp fix](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/0701707d54d1aa79c439717bccc5c282291ed819) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1781))
- [Support mTLS for receptive agents](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/e42ec6b3d740beb375776d7202d30def1cc1a9da) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1764))
- [Use own validator for gRPC server validation](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/d45f35588f285438d508463a1e2b67726cf8a411) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1726))
- [Verify agent id for agent token for receptive agents](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/614f6c1e4075dc93c5d13c9ed5c58e0985898332) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1720))

### Changed (14 changes)

- [Prefer atomic.* types to naked scalars](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/eb9cbdfa085da5c8e8ebf45fd6857812cb3f6aee) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1801))
- [Rely on compiler guarantee for field alignment](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/5be6d59008b406971146c2d7f44c753ea607c4cf) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1801))
- [Check HMAC key sizes](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/ea5ef276d3fb1fadf0ccf72a38f4bdd48d793ef0) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1791))
- [Handle unlicensed receptive agents](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/7b580b3b0f65217ddc037e031475981139fe1566) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1766))
- [Expose default error handlers for reuse](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/fdb2613200278870efa095e09814c825e111efdd) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1766))
- [Improve Sentry event reporting](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/63dc1b0fb08565d760e46871cf5683c2f8f589ab) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1744))
- [Explicitly use the noop meter](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/328a2ec4f63a6b6f3f1fb7012d678bd59f567c28) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1740))
- [Explicitly set gRPC max recv message size](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/a28a66ed3451ed35a56268d97e1f7067cdb4a9b8) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1738))
- [Consistent MakeGitLabRequest() usage](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/3e63537e145e031f9275a2cdf694cf1abc2a6564) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1735))
- [Update receptive agents spec](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/3cc1ff1b056d9c05e91427b42db3f46c49896fe1) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1721))
- [Make workers start/stop/restart independently of each other](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/49c3ffaecebc915a92db0a68f898099e4194f3d6) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1716))
- [Bump linter](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/f55482297d023f3b91964c60b5f8e12c21635fda) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1716))
- [Make it clear that kas config contains default values only](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/6b0f814e56ffeb2be58324dfb8f5480bdcab8984) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1707))
- [nhooyr.io/websocket -> github.com/coder/websocket](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/3c6764abc995dd21a6f342107d22cdb62b6769f1) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1703))

### Performance (13 changes)

- [Limit batch size](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/180fd1184cdc0034847e228fcb237136a43e2a6d) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1797))
- [Batch tunnel refresh](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/7c61a8b410824dfc893778b7375c6c4604f42af1) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1797))
- [Append Git ref to version to disambiguate profiles](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/8b0c7443b0c29f4e2ee1514dd5d8810c95c7e532) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1796))
- [Batch tunnel (un)registration](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/244cc8043f51e768677cb9a788ceb013ee041b74) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1792))
- [IOBuilder.Unset() for multiple keys](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/18fc80e014be0ef0b9ce7d722822ba914bbafe7b) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1792))
- [Allow IOBuilder to be reused](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/e92b6eb2f2bf8b37fe99f30c3cc4a4932573d4bc) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1792))
- [IOBuilder.Unset() to enable batching](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/b0c2b64dbac20504e1025eb6a59059981132a881) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1792))
- [No need for MULTI/EXEC with a single command.](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/c3b629b1ea278b94d2c2f66abf9fa18538b6062c) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1792))
- [RedisIOBuilder.Set() for multiple keys](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/81c79665adffeb7e50fd8eb85ef13ea2815a3015) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1774))
- [Batch tunnel unregistration](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/e1b1735c3da86c04465a3f0ab5c2590d6a45301b) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1774))
- [Use move optimal metadata.ValueFromIncomingContext()](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/566dda368986caabddaf6dbebf295692514bf8ef) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1767))
- [Pass body bytes directly to avoid copying in GitLab client](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/f6e7fcf5347fdaa06bfb56b52e4ea2945c3da11f) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1739))
- [Only run in-memory agent API server when needed](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/853e25f94e69ca30f5e875d34b4964e50417dbe9) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1729))

### Other (6 changes)

- [Bump Go dependencies](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/a78d09fbc34b2b135b02ba9030fbb8bdc3189d21) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1807))
- [Bump Debian images](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/e200823c53c01bc10883ddb2f527f49861d5d697) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1807))
- [Bump Bazel](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/a7b1a0ec0866240c48af7b229504caa5cfcaea49) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1807))
- [Remove temporary logging](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/f38db47c053d8849085d0a208c6b30e939652bfb) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1800))
- [Batch size histogram](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/49dff1b8fc4db5dae51599c30cf60e0663ac9042) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1797))
- [More flexible shutdown ordering machinery](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/18c7ba930d032d98f25469484048861e33f4928a) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1787))

## 17.3.7 (2024-11-12)

No changes.

## 17.3.6 (2024-10-22)

No changes.

## 17.3.5 (2024-10-09)

No changes.

## 17.3.4 (2024-09-24)

No changes.

## 17.3.3 (2024-09-16)

No changes.

## 17.3.2 (2024-09-11)

No changes.

## 17.3.1 (2024-08-20)

No changes.

## 17.3.0 (2024-08-14)

### Added (1 change)

- [Document kas shutdown](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/81deb10fc8ca9dd975274ebb303ab17ec0d40758) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1621))

### Fixed (2 changes)

- [Increase the max backoff and reset duration in agent info retry config](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/341e61ddabb4c1eb314e28b3296b2617fbc180b5) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1638))
- [Fix casing warning in agentk FIPS docker build](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/daca3e9224ec0f1282fb19cc31741109b3121c74) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1626))

### Changed (10 changes)

- [Use Unsafe instead of Unimplemented stubs from gRPC](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/48b7ec6948e670657b93a7540756fd1002982510) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1676))
- [Use library function](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/e656e54f2a6a9dc3d14a8ecf9565e89eb034078d) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1675))
- [Remove authorization header. Rely on Gitlab-Agentk-Api-Request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/32980552209019023454ca19b593e5446550b66e) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1675))
- [Bump rules_proto_grpc](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/fc4cbb982754e516208de3a990afb46c28e9500f) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1674))
- [Use gRPC generic stream types](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/a86a351a9d2b2f46e86ec45b22954d58640732c2) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1672))
- [Bump Go dependencies](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/4f1d3cac327acf1446f263524ec68bd06503a6f0) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1671))
- [Update protoc-gen-go-grpc to new version](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/0799cdb31b0d1e3bd0227248e056fac2bca85929) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1647))
- [PGV -> protovalidate for proto validation](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/87acf0ee3ac5845f098344aa73a646d10d603de4) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1640))
- [Bump WebSocket library](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/d151d7e28d4b41788a7f2df5e9453a23243a9e60) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1603))
- [Use bzlmod for dependency management](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/8339941b366bdc5d8a4c52fabfa677f007359075) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1611))

## 17.2.9 (2024-10-09)

No changes.

## 17.2.8 (2024-09-24)

No changes.

## 17.2.7 (2024-09-16)

No changes.

## 17.2.6 (2024-09-13)

No changes.

## 17.2.5 (2024-09-11)

No changes.

## 17.2.4 (2024-08-21)

No changes.

## 17.2.3 (2024-08-20)

No changes.

## 17.2.2 (2024-08-06)

No changes.

## 17.2.1 (2024-07-24)

No changes.

## 17.2.0 (2024-07-17)

### Added (4 changes)

- [Support after stop callback for gRPC servers](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/5fc3b895910eb6aedc891336127d233de01926a7) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1591))
- [New agentk authn header](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/516eae0f4b384a14671f42d7aa590a5351a17b20) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1592))
- [Support username for Redis Sentinel](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/221901f825928c5c2e8df1ece340ed10b4e92466) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1571))
- [Support kas->agentk connections](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/d11dc332993baca56eec87e224ba3771eb431ff2) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1547))

### Fixed (2 changes)

- [Fixed shutdown errors](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/95e167e36f2c02adb480fed615bc8ec938c7e551) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1601))
- [Fix unsynchronized access](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/ee108ff4fe7c157ac52106b9a53ce45621747688) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1579))

### Changed (7 changes)

- [Move ValueHolder](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/e18574f89a239162c4fbb78e61e7569ef9da42eb) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1606))
- [Bump dependencies](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/9073117f87266efdec8d83cc44f31e72b8e8867b) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1600))
- [zap -> slog for logging](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/a0865860aa8f86dafecff9c0c6c7f7a7aa736c26) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1593))
- [Re-vendored Gitaly client and proto files](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/6fbbfe0a986da93ef8de6850abf40c7d5dcd1d5f) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1574))
- [Regenerate protobuf](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/6fb8c0e5cf96e3f4ba6bdad2e3e9aae300014a87) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1572))
- [Bump dependencies](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/ea854c5f2e7c3e5abc90b861fe06a87278b7516b) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1572))
- [Refactor to use rand v2 API](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/b97b26bf7ec5780b76974544bb9729a00f93b4ed) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1502))

### Removed (2 changes)

- [Remove striping from registry](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/d4f7b2ef67d949fbb382c292f967ff4d3f991039) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1591))
- [Remove unused visitors](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/94a5bff23ffc99e143efb86a24eb0202a8429f0c) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1574))

### Performance (1 change)

- [Asynchronous IO in tunnel registry](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/15d0814e884f80b4db2489ac136ee6c853b553f9) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1591))

### Other (1 change)

- [Upgrade to Go 1.22](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/023d20cfa54092c48180385c84a7f7d8113f97e0) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1502))

## 17.1.8 (2024-09-16)

No changes.

## 17.1.7 (2024-09-11)

No changes.

## 17.1.6 (2024-08-21)

No changes.

## 17.1.5 (2024-08-20)

No changes.

## 17.1.4 (2024-08-06)

No changes.

## 17.1.3 (2024-07-24)

No changes.

## 17.1.2 (2024-07-09)

No changes.

## 17.1.1 (2024-06-25)

### Fixed (1 change)

- [Fix unsynchronized access](https://gitlab.com/gitlab-org/security/cluster-integration/gitlab-agent/-/commit/7ba214f2738d5ba70dca7f6a6e2c0b5427615edb)

## 17.1.0 (2024-06-19)

### Added (2 changes)

- [Support setting log levels in agentk via env vars](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/f9f51d85ccd6df9682f525ff2ec3834a0a3b3abb) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1560))
- [Implement intentional unregistration of an agent](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/a18d2648cd5fda0fe8c1b9d53a52c0a32c82644e) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1550))

### Fixed (4 changes)

- [Fix concurrent access to local agent expiring hash data for GC](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/57dfe7006c88439f8b34b6234cdd3fe0ec8edbc1) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1541))
- [Use separate context for resolving agent infos during error handling](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/639a34115f1bf3c0b60268878c425507cb335d39) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1536))
- [Fix the metric name mix up for routing tunnel timeouts](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/411e5e9329344194460b89d2760a713f710a6b1f) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1525))
- [Fix build date parsing for CentOS 7-based distros](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/b988bbaffb0dc564fb63b33c3bc83aeea2d5830d) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1507))

### Changed (11 changes)

- [Bump Go version](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/63d6258d9ab77defb2a45a8a2bd39be3417c277b) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1566))
- [Update golangci-lint](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/11f8e3335507b672a07c80d40d46d558fc8af26c) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1566))
- [Move profiler enabled check into factory](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/5991fe7230cd6e94355dff5ac0aa096e4f28b8bf) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1566))
- [Bump protoc-gen-go-grpc](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/712810530c5e8fe8d3cddc5bc05cb35aa11458de) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1555))
- [Bump dependencies](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/6690cf74c5a2a2ac32fa4038ef05af4086d9269c) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1555))
- [Go 1.22.4](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/4fd57c761d73e3693d3535f076b4adcacd4995a8) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1555))
- [Move stream preparation into plugin](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/958cb54cd8b5afc731f8f29fb8b5d2f97bd6a20b) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1553))
- [Remove temporary GetConfiguration logs](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/5341aeeb414ec0cf4c0fde7d513c22250ae49987) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1549))
- [Introduce KAS shutdown logs](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/b3cbed00f4cdb0c3d2a9184e0f75f59b7cd0fda4) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1538))
- [Use fields instead of dynamic error message](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/fa0e1477d1dd1ddec26918bfe6517be40673a8f7) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1532))
- [Allow empty msg when reporting an error](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/87eca357700488aaff03b0a060a93e48d2534961) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1532))

### Performance (2 changes)

- [Do not tunnel reserved headers](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/7452592c123eafcc99faa144e17094e934cd1cc7) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1553))
- [Route requests in a ClientConn, remove internal server](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/7ace2a2295d97d83dfff99eabb35eb78af359e39) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1499))

## 17.0.8 (2024-09-16)

No changes.

## 17.0.7 (2024-09-10)

No changes.

## 17.0.6 (2024-08-06)

No changes.

## 17.0.5 (2024-07-24)

No changes.

## 17.0.4 (2024-07-09)

No changes.

## 17.0.3 (2024-06-25)

### Fixed (1 change)

- [Fix concurrent access to local agent expiring hash data for GC](https://gitlab.com/gitlab-org/security/cluster-integration/gitlab-agent/-/commit/2f01679417f9a33c1de4aca9903651561b175ae8)

## 17.0.2 (2024-06-11)

### Fixed (1 change)

- [17.0 backport: Allow text/plain Content-Type in proxy](https://gitlab.com/gitlab-org/security/cluster-integration/gitlab-agent/-/commit/0269509036a455e8aa083c77885acfde2a3d4a29)

### Security (1 change)

- [Ensure required AgentMeta is present](https://gitlab.com/gitlab-org/security/cluster-integration/gitlab-agent/-/commit/28db01e9a414e797daec0cd4c4821376654e5003) ([merge request](https://gitlab.com/gitlab-org/security/cluster-integration/gitlab-agent/-/merge_requests/10))

## 17.0.1 (2024-05-21)

### Fixed (1 change)

- [Fix build date parsing for CentOS 7-based distros](gitlab-org/security/cluster-integration/gitlab-agent@0642f68cd0b7a9046aec97daec722f52b4b209be)

## 17.0.0 (2024-05-15)

### Added (2 changes)

- [Generate Ruby bindings for Server Info RPC](gitlab-org/cluster-integration/gitlab-agent@a4e72b4703a164534aabca74f4b81662276969b5) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1455))
- [Implement Server Info RPC interface](gitlab-org/cluster-integration/gitlab-agent@1f08c0d278f685b59b44231ac2ed5856f4fa7305) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1455))

### Fixed (2 changes)

- [Do not allow to inject version from outside](gitlab-org/cluster-integration/gitlab-agent@1dd0ece91d724bc090c3e3b88433883b906bd96d) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1470))
- [Use -u instead of --utc for date command](gitlab-org/cluster-integration/gitlab-agent@b18d2a66a3280f12ff6049b0678360bf45ca0ace) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1469))

### Changed (4 changes)

- [Drop sigs.k8s.io/controller-runtime dependency](gitlab-org/cluster-integration/gitlab-agent@298927f4512163308de680121eb7cd2937db7223) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1500))
- [Bump Go dependencies](gitlab-org/cluster-integration/gitlab-agent@59f397dde4a8fdd08ede5dc8b40e58686a97df0b) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1500))
- [Use standard datetime format for build time info](gitlab-org/cluster-integration/gitlab-agent@b429f282f0e5ff65686c865814abb1c1eb060b00) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1466))
- [Increase max size of Agent Meta Version string](gitlab-org/cluster-integration/gitlab-agent@203e537e74e1e0bec8ac3231e2a1eb6b607fbeec) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1457))

### Removed (3 changes)

- [Remove GetConnectedAgents gRPC service](gitlab-org/cluster-integration/gitlab-agent@2f349b55ad3b1473d11a477f49ea61e72e584b91) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1433))
- [Remove deprecated gRPC services](gitlab-org/cluster-integration/gitlab-agent@deaaa02c20861140917c184b30ec10fcf1ae3c8c) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1433))
- [Remove `ca-cert-file` CLI option in agentk](gitlab-org/cluster-integration/gitlab-agent@c5ee86f8c38171f20a6347eabff358361b85e657) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1431))

### Other (1 change)

- [Introduce support for VERSION file](gitlab-org/cluster-integration/gitlab-agent@19674f3d5a629086abafa2ec93922c2818ef8139) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1434))

## 16.11.10 (2024-09-16)

No changes.

## 16.11.9 (2024-09-10)

No changes.

## 16.11.8 (2024-08-05)

No changes.

## 16.11.7 (2024-07-23)

No changes.

## 16.11.6 (2024-07-09)

No changes.

## 16.11.5 (2024-06-25)

No changes.

## 16.11.4 (2024-06-11)

### Security (1 change)

- [Ensure required AgentMeta is present or handled when missing](https://gitlab.com/gitlab-org/security/cluster-integration/gitlab-agent/-/commit/0fd9a7d1576246b7e5eb97ec4461c23dafe37051) ([merge request](https://gitlab.com/gitlab-org/security/cluster-integration/gitlab-agent/-/merge_requests/11))

## 16.11.3 (2024-05-21)

No changes.

## 16.11.2 (2024-05-07)

### Fixed (1 change)

- [Do not allow to inject version from outside](gitlab-org/security/cluster-integration/gitlab-agent@141707b32694692d495979eb3191004de3964962)

### Changed (1 change)

- [Increase max size of Agent Meta Version string](gitlab-org/security/cluster-integration/gitlab-agent@ebe741712fd4b178668c3485e1bd99cc041a14f9)

### Other (1 change)

- [Introduce support for VERSION file](gitlab-org/security/cluster-integration/gitlab-agent@aefc4f5a3513fb0d67283ec3d11649a601977d5d)

## 16.11.1 (2024-04-24)

No changes.

## 16.11.0 (2024-04-17)

### Added (1 change)

- [Opt-out option for Flux receiver creation](gitlab-org/cluster-integration/gitlab-agent@81485a3c96ec78b9fb1d96b053669fb467b6fa65)

### Changed (4 changes)

- [Tune Redis COUNT modifier for HSCAN](gitlab-org/cluster-integration/gitlab-agent@2cda802f2e292751aab4ed169ff6961b9281ac54)
- [Only reconcile GitRepository and Receiver when spec changes](gitlab-org/cluster-integration/gitlab-agent@389c01c15c21c1cb1801a5a121d696fc9d8a2890)
- [ScanAndGC -> independent Scan + GC](gitlab-org/cluster-integration/gitlab-agent@49938bd26b220dadd4da9644eb278e01e8b0fe23)
- [Update dependency trivy-k8s-wrapper to v0.2.15](gitlab-org/cluster-integration/gitlab-agent@acce190f70c52c701f1d64248305af47ed1023b3)

### Fixed (4 changes)

- [Stop erroring out when OWN_PRIVATE_API_HOST is not set](gitlab-org/cluster-integration/gitlab-agent@fad847403ffe99cf1cd7df0d6656a40a1737d2e0)
- [Fix garbage collection for keys registered with SetEX](gitlab-org/cluster-integration/gitlab-agent@3fd1c3df686fc07db7fba0ab98c3bf6a7490703b)
- [Fix KAS shutdown issue](gitlab-org/cluster-integration/gitlab-agent@9ace31f290dbef83724abb48d3665cf80f41a083)
- [Don't panic if module exits when config channel is closed](gitlab-org/cluster-integration/gitlab-agent@04d8e67a63c900e68e6aa5bd0f162d20ac3bc643)

## 16.10.10 (2024-09-19)

No changes.

## 16.10.9 (2024-07-23)

No changes.

## 16.10.8 (2024-06-25)

No changes.

## 16.10.7 (2024-06-11)

### Security (1 change)

- [Ensure required AgentMeta is present or handled when missing](https://gitlab.com/gitlab-org/security/cluster-integration/gitlab-agent/-/commit/574f447efc7eca56d73991ae0609809cedd99efd) ([merge request](https://gitlab.com/gitlab-org/security/cluster-integration/gitlab-agent/-/merge_requests/12))

## 16.10.6 (2024-05-21)

No changes.

## 16.10.5 (2024-05-07)

### Fixed (1 change)

- [Do not allow to inject version from outside](gitlab-org/security/cluster-integration/gitlab-agent@cdcf385865c6fb2603f5f4e35cb1199315609274)

### Changed (1 change)

- [Increase max size of Agent Meta Version string](gitlab-org/security/cluster-integration/gitlab-agent@516f2f3232573e8d1eedf2e66e21258a53ba1f2a)

### Other (1 change)

- [Introduce support for VERSION file](gitlab-org/security/cluster-integration/gitlab-agent@0b1112a816876ac08e7161084749aadf4cdf88c7)

## 16.10.4 (2024-04-24)

No changes.

## 16.9.11 (2024-09-19)

No changes.

## 16.9.10 (2024-07-23)

No changes.

## 16.9.8 (2024-05-09)

No changes.

## 16.9.7 (2024-05-07)

### Fixed (1 change)

- [Do not allow to inject version from outside](gitlab-org/security/cluster-integration/gitlab-agent@ccfbedc0b0509fe37250042744d578bc3491325a)

### Changed (1 change)

- [Increase max size of Agent Meta Version string](gitlab-org/security/cluster-integration/gitlab-agent@37a22cc5e72f82f5168314fd62a6734a232ede9c)

### Other (1 change)

- [Introduce support for VERSION file](gitlab-org/security/cluster-integration/gitlab-agent@de9d1e4ea520890b8531335dd006c8c7e3c9c144)

## 16.9.6 (2024-04-24)

No changes.
