package agentkapp

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"log/slog"
	"math/rand/v2"
	"net"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/ash2k/stager"
	"github.com/bufbuild/protovalidate-go"
	"github.com/coder/websocket"
	"github.com/go-logr/logr"
	grpc_prometheus "github.com/grpc-ecosystem/go-grpc-middleware/providers/prometheus"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/collectors"
	"github.com/spf13/cobra"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/cmd"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/api"
	agent_configuration_agent "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_configuration/agent"
	agent_configuration_rpc "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_configuration/rpc"
	agent_registrar_agent "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_registrar/agent"
	agentk2kas_tunnel_agent "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agentk2kas_tunnel/agent"
	flux_agent "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/flux/agent"
	gitlab_access_agent "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/gitlab_access/agent"
	gitlab_access_rpc "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/gitlab_access/rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/gitops/agent/manifestops"
	google_profiler_agent "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/google_profiler/agent"
	kas2agentk_tunnel_agent "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/kas2agentk_tunnel/agent"
	kas2agentk_tunnel_router "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/kas2agentk_tunnel/router"
	kubernetes_api_agent "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/kubernetes_api/agent"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modagent"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
	observability_agent "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/observability/agent"
	remote_development_agent "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/remote_development/agent"
	starboard_vulnerability "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/starboard_vulnerability/agent"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/errz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/grpctool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/httpz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/metric"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/retry"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/syncz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/tlstool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/wstunnel"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tunnel/tunserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/pkg/agentcfg"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/pkg/entity"
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"go.opentelemetry.io/otel"
	otelmetric "go.opentelemetry.io/otel/metric"
	metricnoop "go.opentelemetry.io/otel/metric/noop"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/trace"
	"go.opentelemetry.io/otel/trace/noop"
	"google.golang.org/grpc"
	"google.golang.org/grpc/backoff"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/encoding/gzip"
	"google.golang.org/grpc/grpclog"
	"google.golang.org/grpc/keepalive"
	core_v1 "k8s.io/api/core/v1"
	"k8s.io/cli-runtime/pkg/genericclioptions"
	"k8s.io/client-go/kubernetes/scheme"
	client_core_v1 "k8s.io/client-go/kubernetes/typed/core/v1"
	"k8s.io/client-go/tools/record"
	"k8s.io/klog/v2"
	"k8s.io/kubectl/pkg/cmd/util"
)

const (
	defaultLogLevel     agentcfg.LogLevelEnum = 0 // whatever is 0 is the default value
	defaultGRPCLogLevel                       = agentcfg.LogLevelEnum_error

	defaultAPIListenNetwork = "tcp"

	defaultPrivateAPIListenNetwork = "tcp"
	defaultPrivateAPIListenAddress = ":8081"

	defaultObservabilityListenNetwork = "tcp"
	defaultObservabilityListenAddress = ":8080"
	agentName                         = "gitlab-agent"

	envVarPodNamespace       = "POD_NAMESPACE"
	envVarPodName            = "POD_NAME"
	envVarPodLabelSelector   = "POD_SELECTOR_LABELS"
	envVarServiceAccountName = "SERVICE_ACCOUNT_NAME"
	envVarAgentkToken        = "AGENTK_TOKEN"
	envVarGRPCLogLevel       = "GRPC_GO_LOG_SEVERITY_LEVEL" // intentionally the same name as gRPC uses
	envVarLogLevel           = "LOG_LEVEL"

	getConfigurationInitBackoff   = 10 * time.Second
	getConfigurationMaxBackoff    = 5 * time.Minute
	getConfigurationResetDuration = 10 * time.Minute
	getConfigurationBackoffFactor = 2.0
	getConfigurationJitter        = 5.0

	routingAttemptInterval      = 50 * time.Millisecond
	routingInitBackoff          = 100 * time.Millisecond
	routingMaxBackoff           = 1 * time.Second
	routingResetDuration        = 10 * time.Second
	routingBackoffFactor        = 2.0
	routingJitter               = 5.0
	routingTunnelFindTimeout    = 20 * time.Second
	routingTryNewAgentkInterval = 10 * time.Millisecond
)

type obsTools struct {
	reg              *prometheus.Registry
	tp               trace.TracerProvider
	mp               otelmetric.MeterProvider
	p                propagation.TextMapPropagator
	streamProm       grpc.StreamServerInterceptor
	unaryProm        grpc.UnaryServerInterceptor
	streamClientProm grpc.StreamClientInterceptor
	unaryClientProm  grpc.UnaryClientInterceptor
}

type options struct {
	configFlags *genericclioptions.ConfigFlags

	log                *slog.Logger
	logLevel           *slog.LevelVar
	grpcHandler        slog.Handler
	grpcLogLevel       *slog.LevelVar
	validator          protovalidate.Validator
	agentMeta          *entity.AgentMeta
	agentID            *syncz.ValueHolder[int64]
	gitLabExternalURL  *syncz.ValueHolder[url.URL]
	serviceAccountName string

	// kasAddress specifies the address of kas.
	// This field is used as a flag. If set, agentk is in the agentk->kas communication mode.
	// If not set, kas->agentk mode is activated.
	kasAddress       string
	kasCACertFile    string
	kasHeaders       []string
	kasSkipTLSVerify bool
	kasTLSServerName string

	apiListenNetwork string
	apiListenAddress string
	apiCertFile      string
	apiKeyFile       string
	apiJWTFile       string
	apiCACertFile    string
	apiMTLS          bool

	privateAPIListenNetwork string
	privateAPIListenAddress string
	privateAPICertFile      string
	privateAPIKeyFile       string
	privateAPICACertFile    string
	privateAPIJWTFile       string

	observabilityListenNetwork string
	observabilityListenAddress string
	observabilityCertFile      string
	observabilityKeyFile       string

	tokenFile  string
	agentToken api.AgentToken
}

func newOptions() *options {
	return &options{
		configFlags: genericclioptions.NewConfigFlags(true),
		agentMeta: &entity.AgentMeta{
			Version:           cmd.Version,
			CommitId:          cmd.GitRef, // For backwards compatibility.
			PodNamespace:      os.Getenv(envVarPodNamespace),
			PodName:           os.Getenv(envVarPodName),
			KubernetesVersion: &entity.KubernetesVersion{},
			GitRef:            cmd.GitRef,
		},
		agentID:                    syncz.NewValueHolder[int64](),
		gitLabExternalURL:          syncz.NewValueHolder[url.URL](),
		serviceAccountName:         os.Getenv(envVarServiceAccountName),
		apiListenNetwork:           defaultAPIListenNetwork,
		privateAPIListenNetwork:    defaultPrivateAPIListenNetwork,
		privateAPIListenAddress:    defaultPrivateAPIListenAddress,
		observabilityListenNetwork: defaultObservabilityListenNetwork,
		observabilityListenAddress: defaultObservabilityListenAddress,
	}
}

func (o *options) complete() error {
	stderr := &logz.LockedWriter{Writer: os.Stderr}
	handler, logLevel, err := o.logHandler(envVarLogLevel, defaultLogLevel, stderr)
	if err != nil {
		return err
	}
	o.log = slog.New(handler)
	o.logLevel = logLevel

	o.grpcHandler, o.grpcLogLevel, err = o.logHandler(envVarGRPCLogLevel, defaultGRPCLogLevel, stderr)
	if err != nil {
		return err
	}

	o.validator, err = protovalidate.New()
	if err != nil {
		return err
	}

	tokenFromEnv, ok := os.LookupEnv(envVarAgentkToken)
	switch {
	case o.tokenFile != "" && ok:
		return fmt.Errorf("unable to use both token file and %s environment variable to set the agent token", envVarAgentkToken)
	case o.tokenFile != "":
		tokenData, err := os.ReadFile(o.tokenFile)
		if err != nil {
			return fmt.Errorf("token file: %w", err)
		}
		tokenData = bytes.TrimSuffix(tokenData, []byte{'\n'})
		o.agentToken = api.AgentToken(tokenData)
	case ok:
		o.agentToken = api.AgentToken(tokenFromEnv)
		err := os.Unsetenv(envVarAgentkToken)
		if err != nil {
			return fmt.Errorf("failed to unset env var: %w", err)
		}
	default:
		return fmt.Errorf("agent token not set. Please set either token file or %s environment variable", envVarAgentkToken)
	}

	return nil
}

func (o *options) validate() error {
	err := o.validator.Validate(o.agentMeta) // validate the constraints in the proto
	if err != nil {
		return fmt.Errorf("invalid agent metadata. Make sure to set %s and %s environment variables correctly: %w",
			envVarPodNamespace, envVarPodName, err)
	}
	// Extra validation. proto has to be backwards compatible with old agents, so server cannot enforce non-empty namespace and name.
	// Hence, manual validation on the client side in new agents.
	if o.agentMeta.PodNamespace == "" {
		return fmt.Errorf("%s environment variable is required but is empty", envVarPodNamespace)
	}
	if o.agentMeta.PodName == "" {
		return fmt.Errorf("%s environment variable is required but is empty", envVarPodName)
	}
	return nil
}

// setGlobals shouldn't exist, yet here we are.
func (o *options) setGlobals() {
	grpclog.SetLoggerV2(&grpctool.Logger{Handler: o.grpcHandler}) // pipe gRPC logs into slog
	logrLogger := logr.FromSlogHandler(o.log.Handler())
	// Kubernetes uses klog so here we pipe all logs from it to our logger via an adapter.
	klog.SetLogger(logrLogger)
	otel.SetLogger(logrLogger)
	otel.SetErrorHandler((*metric.OtelErrorHandler)(o.log))
}

func (o *options) run(ctx context.Context) (retErr error) {
	ot, stop, err := o.constructObservabilityTools()
	if err != nil {
		return err
	}
	defer errz.SafeCall(stop, &retErr)

	// Construct Kubernetes tools.
	k8sFactory := util.NewFactory(o.configFlags)
	kubeClient, err := k8sFactory.KubernetesClientSet()
	if err != nil {
		return err
	}

	// Construct event recorder
	eventBroadcaster := record.NewBroadcaster()
	eventRecorder := eventBroadcaster.NewRecorder(scheme.Scheme, core_v1.EventSource{Component: agentName})

	// Construct leader runner
	lr := newLeaderRunner(&leaseLeaderElector{
		namespace: o.agentMeta.PodNamespace,
		name: func(ctx context.Context) (string, error) {
			id, err := o.agentID.Get(ctx)
			if err != nil {
				return "", err
			}
			// We use agent id as part of lock name so that agentk Pods of different id don't compete with
			// each other. Only Pods with same agent id should compete for a lock. Put differently, agentk Pods
			// with same agent id have the same lock name but with different id have different lock name.
			return fmt.Sprintf("agent-%d-lock", id), nil
		},
		identity:           o.agentMeta.PodName,
		coordinationClient: kubeClient.CoordinationV1(),
		eventRecorder:      eventRecorder,
	})

	rpcAPIFactory := o.newRPCAPIFactory()

	// ========= things that depend on the mode of operation of the reverse tunnel: kas->agentk / agentk->kas :: START
	var kasConn grpc.ClientConnInterface
	var apiServer grpctool.GRPCServer
	var apiServerStart func(stager.Stage)
	var privateAPIServerStart func(stager.Stage)
	var registerKASAPI func(*grpc.ServiceDesc)
	var tunnelFactory modagent.Factory
	var runQuerier func(ctx context.Context) error

	if o.kasAddress == "" {
		var err error
		sharedAPI := &agentSharedAPI{}

		apiSrv, err := newListenAPIServer( //nolint:contextcheck
			o.log, ot, rpcAPIFactory,
			o.apiListenNetwork, o.apiListenAddress,
			o.apiCertFile, o.apiKeyFile,
			o.apiJWTFile, api.JWTKAS, api.JWTAgentk,
			o.apiCACertFile, o.apiMTLS,
			o.validator,
		)
		if err != nil {
			return fmt.Errorf("API server: %w", err)
		}

		privateSrv, err := newPrivateAPIServer( //nolint:contextcheck
			o.log,
			ot,
			modshared.APIToErrReporter(sharedAPI),
			rpcAPIFactory,
			o.validator,
			o.agentkName(),
			o.privateAPIListenNetwork, o.privateAPIListenAddress,
			o.privateAPICertFile, o.privateAPIKeyFile, o.privateAPICACertFile,
			o.privateAPIJWTFile,
		)
		if err != nil {
			return fmt.Errorf("private API server: %w", err)
		}
		defer errz.SafeClose(privateSrv, &retErr)

		podLabelSelector := os.Getenv(envVarPodLabelSelector)
		o.log.Info("Using Pod label selector to find agentk Pods within the namespace",
			logz.PodNamespace(o.agentMeta.PodNamespace), logz.LabelSelector(podLabelSelector))
		gq := kas2agentk_tunnel_router.NewGatewayURLQuerier(
			kubeClient,
			o.agentMeta.PodNamespace,
			podLabelSelector,
			privateSrv.ownURLScheme,
			privateSrv.ownURLPort,
		)
		registry := kas2agentk_tunnel_router.NewRegistry(o.log)
		plugin := &kas2agentk_tunnel_router.Plugin{
			Registry:         registry,
			AgentkPool:       privateSrv.agentkPool,
			GatewayQuerier:   gq,
			API:              sharedAPI,
			OwnPrivateAPIURL: privateSrv.ownURL,
			Creds:            grpctool.NewTokenCredentials(o.agentToken, true),
			PollConfig: retry.NewPollConfigFactory(routingAttemptInterval, retry.NewExponentialBackoffFactory(
				routingInitBackoff,
				routingMaxBackoff,
				routingResetDuration,
				routingBackoffFactor,
				routingJitter,
			)),
			TryNewGatewayInterval: routingTryNewAgentkInterval,
			TunnelFindTimeout:     routingTunnelFindTimeout,
		}
		kasConn = &tunserver.RoutingClientConn[grpctool.URLTarget]{
			Log:    o.log,
			API:    sharedAPI,
			Plugin: plugin,
		}
		apiServer = apiSrv.server
		apiServerStart = apiSrv.Start
		privateAPIServerStart = privateSrv.Start
		registerKASAPI = (&tunserver.Router[grpctool.URLTarget]{
			Plugin:           plugin,
			PrivateAPIServer: privateSrv.server,
		}).RegisterTunclientAPI
		tunnelFactory = &kas2agentk_tunnel_agent.Factory{
			Registry: registry,
		}
		runQuerier = gq.Run
	} else {
		var err error
		// Construct gRPC connection to gitlab-kas
		realKASConn, err := o.constructKASConnection(ot)
		if err != nil {
			return err
		}
		defer errz.SafeClose(realKASConn, &retErr)

		// Construct in-mem API gRPC server
		apiSrv, err := newInMemAPIServer(ot, rpcAPIFactory, o.validator)
		if err != nil {
			return fmt.Errorf("in-mem API server: %w", err)
		}
		defer errz.SafeClose(apiSrv, &retErr)

		kasConn = realKASConn
		apiServer = apiSrv.server
		registerKASAPI = func(desc *grpc.ServiceDesc) {}
		apiServerStart = apiSrv.Start
		privateAPIServerStart = func(stage stager.Stage) {}
		tunnelFactory = &agentk2kas_tunnel_agent.Factory{
			APIServerConn: apiSrv.inMemConn,
		}
		runQuerier = func(ctx context.Context) error {
			return nil
		}
	}
	// ========= things that depend on the mode of operation of the reverse tunnel: kas->agentk / agentk->kas :: END

	// Start events processing pipeline.
	loggingWatch := eventBroadcaster.StartStructuredLogging(0)
	defer loggingWatch.Stop()
	eventBroadcaster.StartRecordingToSink(&client_core_v1.EventSinkImpl{Interface: kubeClient.CoreV1().Events("")})
	defer eventBroadcaster.Shutdown()

	instanceID := rand.Int64() //nolint: gosec
	runner := o.newModuleRunner(kasConn)
	accessClient := gitlab_access_rpc.NewGitlabAccessClient(kasConn)
	configForFactory := func(name string) *modagent.Config {
		return &modagent.Config{
			Log:        o.log.With(logz.ModuleName(name)),
			InstanceID: instanceID,
			AgentMeta:  o.agentMeta,
			API: &agentAPI{
				moduleName:        name,
				agentID:           o.agentID,
				gitLabExternalURL: o.gitLabExternalURL,
				client:            accessClient,
			},
			K8sUtilFactory:     k8sFactory,
			KASConn:            kasConn,
			APIServer:          apiServer,
			RegisterKASAPI:     registerKASAPI,
			AgentName:          agentName,
			AgentNameVersion:   fmt.Sprintf("%s/%s/%s", agentName, o.agentMeta.Version, o.agentMeta.GitRef),
			ServiceAccountName: o.serviceAccountName,
			Validator:          o.validator,
		}
	}

	// Start things up. Stages are shut down in reverse order.
	return syncz.RunLayers(ctx,
		syncz.LayerGoFunc(func(ctx context.Context) error {
			// Start leader runner.
			lr.Run(ctx)
			return nil
		}),
		syncz.SiblingLayers{
			// Start querier. Tunnel routing depends on it so it should stop after servers.
			syncz.LayerGoFunc(runQuerier),
			&factoriesLayer{
				log:    o.log,
				config: configForFactory,
				lr:     lr,
				mr:     runner,
				factories: []modagent.Factory{
					&agent_configuration_agent.Factory{},
					&gitlab_access_agent.Factory{},
					&google_profiler_agent.Factory{},
					&kubernetes_api_agent.Factory{},
					&manifestops.Factory{},
					&observability_agent.Factory{
						LogLevel:            o.logLevel,
						GRPCLogLevel:        o.grpcLogLevel,
						DefaultGRPCLogLevel: defaultGRPCLogLevel,
						Gatherer:            ot.reg,
						Registerer:          ot.reg,
						ListenNetwork:       o.observabilityListenNetwork,
						ListenAddress:       o.observabilityListenAddress,
						CertFile:            o.observabilityCertFile,
						KeyFile:             o.observabilityKeyFile,
					},
					&remote_development_agent.Factory{},
					&starboard_vulnerability.Factory{},
				},
			},
		},
		syncz.LayerFunc(func(stage stager.Stage) {
			// Start servers.
			privateAPIServerStart(stage)
			apiServerStart(stage)
		}),
		&factoriesLayer{
			log:    o.log,
			config: configForFactory,
			lr:     lr,
			mr:     runner,
			factories: []modagent.Factory{
				// Uses kas connection.
				&agent_registrar_agent.Factory{},
				// Uses kas connection.
				&flux_agent.Factory{},
				// Uses kas connection.
				tunnelFactory,
			},
		},
		// Start configuration refresh.
		syncz.LayerGoFunc(runner.RunConfigurationRefresh),
	)
}

func (o *options) newRPCAPIFactory() modshared.RPCAPIFactory {
	return func(ctx context.Context, method string) modshared.RPCAPI {
		service, method := grpctool.SplitGRPCMethod(method)
		return &agentRPCAPI{
			RPCAPIStub: modshared.RPCAPIStub{
				Logger:    o.log.With(logz.TraceIDFromContext(ctx), logz.GRPCService(service), logz.GRPCMethod(method)),
				StreamCtx: ctx,
			},
		}
	}
}

func (o *options) newModuleRunner(kasConn grpc.ClientConnInterface) *moduleRunner {
	return &moduleRunner{
		log: o.log,
		configurationWatcher: &agent_configuration_rpc.ConfigurationWatcher{
			Log:    o.log,
			Client: agent_configuration_rpc.NewAgentConfigurationClient(kasConn),
			PollConfig: retry.NewPollConfigFactory(0, retry.NewExponentialBackoffFactory(
				getConfigurationInitBackoff,
				getConfigurationMaxBackoff,
				getConfigurationResetDuration,
				getConfigurationBackoffFactor,
				getConfigurationJitter,
			)),
			ConfigPreProcessor: func(data agent_configuration_rpc.ConfigurationData) error {
				err := o.agentID.Set(data.Config.AgentId)
				if err != nil {
					return err
				}
				u, err := url.Parse(data.Config.GitlabExternalUrl)
				if err != nil {
					return fmt.Errorf("unable to parse configured GitLab External URL %q: %w", data.Config.GitlabExternalUrl, err)
				}
				return o.gitLabExternalURL.Set(*u)
			},
		},
	}
}

func (o *options) constructKASConnection(ot *obsTools) (*grpc.ClientConn, error) {
	tlsConfig, err := tlstool.ClientConfigWithCACert(o.kasCACertFile)
	if err != nil {
		return nil, err
	}
	tlsConfig.InsecureSkipVerify = o.kasSkipTLSVerify
	tlsConfig.ServerName = o.kasTLSServerName
	u, err := url.Parse(o.kasAddress)
	if err != nil {
		return nil, fmt.Errorf("invalid gitlab-kas address: %w", err)
	}
	kasHeaders, err := parseHeaders(o.kasHeaders)
	if err != nil {
		return nil, err
	}
	userAgent := o.agentkName()
	opts := []grpc.DialOption{
		grpc.WithStatsHandler(otelgrpc.NewClientHandler(
			otelgrpc.WithTracerProvider(ot.tp),
			otelgrpc.WithMeterProvider(ot.mp),
			otelgrpc.WithPropagators(ot.p),
			otelgrpc.WithMessageEvents(otelgrpc.ReceivedEvents, otelgrpc.SentEvents),
		)),
		// Default gRPC parameters are good, no need to change them at the moment.
		// Specify them explicitly for discoverability.
		// See https://github.com/grpc/grpc/blob/master/doc/connection-backoff.md.
		grpc.WithConnectParams(grpc.ConnectParams{
			Backoff:           backoff.DefaultConfig,
			MinConnectTimeout: 20 * time.Second, // matches the default gRPC value.
		}),
		grpc.WithSharedWriteBuffer(true),
		grpc.WithDefaultCallOptions(grpc.UseCompressor(gzip.Name), grpc.MaxCallRecvMsgSize(api.GRPCMaxMessageSize)),
		grpc.WithUserAgent(userAgent),
		// keepalive.ClientParameters must be specified at least as large as what is allowed by the
		// server-side grpc.KeepaliveEnforcementPolicy
		grpc.WithKeepaliveParams(keepalive.ClientParameters{
			// kas allows min 20 seconds, trying to stay below 60 seconds (typical load-balancer timeout) and
			// above kas' server keepalive Time so that kas pings the client sometimes. This helps mitigate
			// reverse-proxies' enforced server response timeout.
			Time:                55 * time.Second,
			PermitWithoutStream: true,
		}),
		grpc.WithChainStreamInterceptor(
			ot.streamClientProm,
			grpctool.StreamClientValidatingInterceptor(o.validator),
		),
		grpc.WithChainUnaryInterceptor(
			ot.unaryClientProm,
			grpctool.UnaryClientValidatingInterceptor(o.validator),
		),
	}
	var addressToDial string
	// "grpcs" is the only scheme where encryption is done by gRPC.
	// "wss" is secure too but gRPC cannot know that, so we tell it it's not.
	secure := u.Scheme == "grpcs"
	switch u.Scheme {
	case "ws", "wss":
		addressToDial = "passthrough:" + o.kasAddress
		dialer := net.Dialer{
			Timeout:   30 * time.Second,
			KeepAlive: 30 * time.Second,
		}
		kasHeaders.Set(httpz.UserAgentHeader, userAgent)
		kasHeaders.Set(httpz.AuthorizationHeader, "Bearer "+string(o.agentToken))

		opts = append(opts, grpc.WithContextDialer(wstunnel.DialerForGRPC(api.WebSocketMaxMessageSize, &websocket.DialOptions{
			HTTPClient: &http.Client{
				Transport: &http.Transport{
					Proxy:                 http.ProxyFromEnvironment,
					DialContext:           dialer.DialContext,
					TLSClientConfig:       tlsConfig,
					MaxIdleConns:          10,
					IdleConnTimeout:       90 * time.Second,
					TLSHandshakeTimeout:   10 * time.Second,
					ResponseHeaderTimeout: 20 * time.Second,
				},
				CheckRedirect: func(req *http.Request, via []*http.Request) error {
					return http.ErrUseLastResponse
				},
			},
			HTTPHeader:      kasHeaders,
			CompressionMode: websocket.CompressionDisabled,
		})))
	case "grpc":
		// See https://github.com/grpc/grpc/blob/master/doc/naming.md.
		addressToDial = "dns:" + grpctool.HostWithPort(u)
		opts = append(opts,
			grpc.WithPerRPCCredentials(grpctool.NewHeaderMetadata(kasHeaders, !secure)),
			// See https://github.com/grpc/grpc/blob/master/doc/service_config.md.
			// See https://github.com/grpc/grpc/blob/master/doc/load-balancing.md.
			grpc.WithDefaultServiceConfig(`{"loadBalancingConfig":[{"round_robin":{}}]}`),
		)
	case "grpcs":
		// See https://github.com/grpc/grpc/blob/master/doc/naming.md.
		addressToDial = "dns:" + grpctool.HostWithPort(u)
		opts = append(opts,
			grpc.WithTransportCredentials(credentials.NewTLS(tlsConfig)),
			grpc.WithPerRPCCredentials(grpctool.NewHeaderMetadata(kasHeaders, !secure)),
			// See https://github.com/grpc/grpc/blob/master/doc/service_config.md.
			// See https://github.com/grpc/grpc/blob/master/doc/load-balancing.md.
			grpc.WithDefaultServiceConfig(`{"loadBalancingConfig":[{"round_robin":{}}]}`),
		)
	default:
		return nil, fmt.Errorf("unsupported scheme in GitLab Kubernetes Agent Server address: %q", u.Scheme)
	}
	if !secure {
		opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))
	}
	opts = append(opts, grpc.WithPerRPCCredentials(grpctool.NewTokenCredentials(o.agentToken, !secure)))
	conn, err := grpc.NewClient(addressToDial, opts...)
	if err != nil {
		return nil, fmt.Errorf("gRPC.dial: %w", err)
	}
	return conn, nil
}

func (o *options) constructObservabilityTools() (ot *obsTools, stop func() error, retErr error) {
	// Metrics
	reg := prometheus.NewPedanticRegistry()
	goCollector := collectors.NewGoCollector()
	procCollector := collectors.NewProcessCollector(collectors.ProcessCollectorOpts{})
	srvProm := grpc_prometheus.NewServerMetrics()
	clientProm := grpc_prometheus.NewClientMetrics()
	err := metric.Register(reg, goCollector, procCollector, srvProm, clientProm)
	if err != nil {
		return nil, nil, err
	}
	streamProm := srvProm.StreamServerInterceptor()
	unaryProm := srvProm.UnaryServerInterceptor()
	streamClientProm := clientProm.StreamClientInterceptor()
	unaryClientProm := clientProm.UnaryClientInterceptor()

	// TODO Tracing
	tp := noop.NewTracerProvider()
	p := propagation.NewCompositeTextMapPropagator(propagation.TraceContext{}, propagation.Baggage{})

	// TODO metrics via OTEL
	mp := metricnoop.NewMeterProvider()

	return &obsTools{
			reg:              reg,
			tp:               tp,
			mp:               mp,
			p:                p,
			streamProm:       streamProm,
			unaryProm:        unaryProm,
			streamClientProm: streamClientProm,
			unaryClientProm:  unaryClientProm,
		}, func() error {
			return nil
		}, nil
}

// agentkName return a string to use as User-Agent or Server HTTP header.
func (o *options) agentkName() string {
	return fmt.Sprintf("%s/%s/%s", agentName, o.agentMeta.Version, o.agentMeta.GitRef)
}

func (o *options) logHandler(envVar string, levelEnum agentcfg.LogLevelEnum, writer io.Writer) (slog.Handler, *slog.LevelVar, error) {
	l := os.Getenv(envVar)
	if l == "" {
		l = levelEnum.String()
	}
	levelVar := &slog.LevelVar{}
	err := levelVar.UnmarshalText([]byte(l))
	if err != nil {
		return nil, nil, err
	}

	handler := &agentIDHandler{
		delegate: slog.NewJSONHandler(writer, &slog.HandlerOptions{
			Level: levelVar,
		}),
		agentID: o.agentID,
	}
	return handler, levelVar, nil
}

func NewCommand() *cobra.Command {
	o := newOptions()
	c := &cobra.Command{
		Use:   "agentk",
		Short: "GitLab Agent for Kubernetes",
		Args:  cobra.NoArgs,
		Run: func(c *cobra.Command, args []string) {
			cmd.LogAndExitOnError(o.log, o.complete())
			cmd.LogAndExitOnError(o.log, o.validate())
			o.setGlobals()
			cmd.LogAndExitOnError(o.log, o.run(c.Context()))
		},
		SilenceErrors: true, // Don't want Cobra to print anything, we handle error printing/logging ourselves.
		SilenceUsage:  true,
	}
	f := c.Flags()
	f.StringVar(&o.kasAddress, "kas-address", o.kasAddress, "GitLab Kubernetes Agent Server address")
	f.StringVar(&o.tokenFile, "token-file", o.tokenFile, "File with access token")

	f.StringVar(&o.kasCACertFile, "kas-ca-cert-file", o.kasCACertFile, "File with X.509 certificate authority certificate in PEM format. Used for verifying cert of KAS (GitLab Kubernetes Agent Server)")
	f.StringArrayVar(&o.kasHeaders, "kas-header", o.kasHeaders, "HTTP headers to set when connecting to the agent server")
	f.BoolVar(&o.kasSkipTLSVerify, "kas-insecure-skip-tls-verify", o.kasSkipTLSVerify, "If true, the agent server's certificate will not be checked for validity. This will make the connection insecure")
	f.StringVar(&o.kasTLSServerName, "kas-tls-server-name", o.kasTLSServerName, "Server name to use for agent server certificate validation. If it is not provided, the hostname used to contact the server is used")

	f.StringVar(&o.apiListenNetwork, "api-listen-network", o.apiListenNetwork, "API network to listen on")
	f.StringVar(&o.apiListenAddress, "api-listen-address", o.apiListenAddress, "API address to listen on")
	f.StringVar(&o.apiCertFile, "api-cert-file", o.apiCertFile, "File with X.509 certificate in PEM format for API endpoint TLS")
	f.StringVar(&o.apiKeyFile, "api-key-file", o.apiKeyFile, "File with X.509 key in PEM format for API endpoint TLS")
	f.StringVar(&o.apiJWTFile, "api-jwt-file", o.apiJWTFile, "Base64-encoded EdDSA public key to validate JWT tokens from kas. Used for API endpoint")
	f.StringVar(&o.apiCACertFile, "api-client-ca-cert-file", o.apiCACertFile, "File with X.509 certificate authority certificate in PEM format. Used for verifying client cert of KAS (GitLab Kubernetes Agent Server). This is used for kas->agentk mTLS")
	f.BoolVar(&o.apiMTLS, "api-mtls", o.apiMTLS, "Used to enabled kas->agentk mTLS. Defaults to true when --api-client-ca-cert-file is specified")
	c.MarkFlagsRequiredTogether("api-cert-file", "api-key-file")
	c.MarkFlagsMutuallyExclusive("api-jwt-file", "api-client-ca-cert-file")

	f.StringVar(&o.privateAPIListenNetwork, "private-api-listen-network", o.privateAPIListenNetwork, "Private API network to listen on")
	f.StringVar(&o.privateAPIListenAddress, "private-api-listen-address", o.privateAPIListenAddress, "Private API address to listen on")
	f.StringVar(&o.privateAPICertFile, "private-api-cert-file", o.privateAPICertFile, "File with X.509 certificate in PEM format for private API endpoint TLS")
	f.StringVar(&o.privateAPIKeyFile, "private-api-key-file", o.privateAPIKeyFile, "File with X.509 key in PEM format for private API endpoint TLS")
	f.StringVar(&o.privateAPICACertFile, "private-api-ca-cert-file", o.privateAPICACertFile, "File with X.509 certificate authority certificate in PEM format for private API endpoint TLS")
	f.StringVar(&o.privateAPIJWTFile, "private-api-jwt-file", o.privateAPIJWTFile, "Base64-encoded secret for JWT token creation and validation for agentk to agentk private API access")
	c.MarkFlagsRequiredTogether("private-api-cert-file", "private-api-key-file")

	f.StringVar(&o.observabilityListenNetwork, "observability-listen-network", o.observabilityListenNetwork, "Observability network to listen on")
	f.StringVar(&o.observabilityListenAddress, "observability-listen-address", o.observabilityListenAddress, "Observability address to listen on")
	f.StringVar(&o.observabilityCertFile, "observability-cert-file", o.observabilityCertFile, "File with X.509 certificate in PEM format for observability endpoint TLS")
	f.StringVar(&o.observabilityKeyFile, "observability-key-file", o.observabilityKeyFile, "File with X.509 key in PEM format for observability endpoint TLS")
	c.MarkFlagsRequiredTogether("observability-cert-file", "observability-key-file")

	o.configFlags.AddFlags(f)
	c.MarkFlagsOneRequired("kas-address", "api-listen-address")
	c.MarkFlagsMutuallyExclusive("kas-address", "api-listen-address")

	return c
}

func parseHeaders(raw []string) (http.Header, error) {
	header := http.Header{}
	for _, h := range raw {
		k, v, ok := strings.Cut(h, ":")
		if !ok {
			return nil, fmt.Errorf("invalid header supplied: %s", h)
		}
		k, v = strings.Trim(k, " "), strings.Trim(v, " ")
		if len(k) < 1 || len(v) < 1 {
			return nil, fmt.Errorf("invalid header supplied: %s", h)
		}
		header.Add(k, v)
	}
	return header, nil
}
