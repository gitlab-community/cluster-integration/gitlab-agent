package runlocal

import "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/autoflow/flow"

var (
	_ flow.Builtins            = (*localFlowBuiltins)(nil)
	_ flow.EventHandlerContext = (*localFlowContext)(nil)
	_ flow.Future              = (*localTimer)(nil)
)
