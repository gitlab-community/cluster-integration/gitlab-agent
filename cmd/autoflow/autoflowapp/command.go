package autoflowapp

import (
	"log/slog"
	"os"

	"github.com/spf13/cobra"
	runlocal "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/cmd/autoflow/autoflowapp/commands/run-local"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
)

func NewCommand() *cobra.Command {
	lockedWriter := &logz.LockedWriter{
		Writer: os.Stderr,
	}
	log := slog.New(slog.NewTextHandler(lockedWriter, &slog.HandlerOptions{Level: slog.LevelDebug}))
	c := &cobra.Command{
		Use:          "autoflow",
		Short:        "GitLab AutoFlow command line utility",
		Args:         cobra.NoArgs,
		SilenceUsage: true,
	}
	c.AddCommand(runlocal.NewCommand(log))
	return c
}
