## Contributor license agreement

By contributing to GitLab Inc., You accept and agree to the following terms and
conditions for Your present and future Contributions submitted to GitLab Inc.
Except for the license granted herein to GitLab Inc. and recipients of software
distributed by GitLab Inc., You reserve all right, title, and interest in and to
Your Contributions. All Contributions are subject to the following DCO + License
terms.

[DCO + License](https://gitlab.com/gitlab-org/dco/blob/master/README.md)

All Documentation content that resides under the [doc/ directory](/doc) of this
repository is licensed under Creative Commons:
[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

_This notice should stay as the first item in the CONTRIBUTING.md file._

## Code of Conduct

Please see the [Code of Conduct](https://about.gitlab.com/contributing/code-of-conduct/) that is applicable for this project.

## How to contribute

The [GitLab Agent for Kubernetes Community Fork](https://gitlab.com/gitlab-community/cluster-integration/gitlab-agent) includes a functioning CI/CD pipeline for your contributions. To start using the GitLab Community Fork, see [GitLab Community Forks - How to](https://gitlab.com/gitlab-community/meta#how-to).

To set up your development environment, see the [Development guide](doc/developing.md).
