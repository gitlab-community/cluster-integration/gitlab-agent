package kascfg_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/cmd/kas/kasapp"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/matcher"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/pkg/kascfg"
	"google.golang.org/protobuf/encoding/protojson"
	"sigs.k8s.io/yaml"
)

const (
	kasConfigDefaultsFile = "kascfg_defaults.yaml"
)

func TestExampleConfigHasCorrectDefaults(t *testing.T) {
	cfgDefaulted := &kascfg.ConfigurationFile{}
	kasapp.ApplyDefaultsToKASConfigurationFile(cfgDefaulted)

	printCorrectYAML := false

	cfgFromFile, err := kasapp.LoadConfigurationFile(context.Background(), kasConfigDefaultsFile)
	if assert.NoError(t, err) {
		if !matcher.AssertProtoEqual(t, cfgDefaulted, cfgFromFile) {
			printCorrectYAML = true
		}
	} else {
		printCorrectYAML = true
	}
	if printCorrectYAML {
		// Failed to load. Just print what it should be
		data, err := protojson.Marshal(cfgDefaulted)
		require.NoError(t, err)
		configYAML, err := yaml.JSONToYAML(data)
		require.NoError(t, err)
		fmt.Println(string(configYAML)) //nolint: forbidigo
	}
}
