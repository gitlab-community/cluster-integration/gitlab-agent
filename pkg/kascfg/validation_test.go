package kascfg

import (
	"testing"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/testhelpers"
	"google.golang.org/protobuf/types/known/durationpb"
)

func TestValidation_Valid(t *testing.T) {
	tests := []testhelpers.ValidTestcase{
		{
			Name: "minimal",
			Valid: &ConfigurationFile{
				Gitlab: &GitLabCF{
					Address:                  "http://localhost:8080",
					AuthenticationSecretFile: "/some/file",
				},
				Redis: &RedisCF{
					RedisConfig: &RedisCF_Sentinel{
						Sentinel: &RedisSentinelCF{
							MasterName: "redis",
							Addresses:  []string{"redis:6379"},
						},
					},
				},
				Api: &ApiCF{
					Listen: &ListenApiCF{
						AuthenticationSecretFile: "/some/file",
					},
				},
				PrivateApi: &PrivateApiCF{
					Listen: &ListenPrivateApiCF{
						AuthenticationSecretFile: "/some/file",
					},
				},
			},
		},
		{
			Name: "AgentCF",
			Valid: &AgentCF{
				InfoCacheTtl:         durationpb.New(0), // zero means "disabled"
				RedisConnInfoRefresh: durationpb.New(1),
				RedisConnInfoTtl:     durationpb.New(2),
			},
		},
		{
			Name: "ObservabilityCF",
			Valid: &ObservabilityCF{
				UsageReportingPeriod: durationpb.New(0), // zero means "disabled"
			},
		},
		{
			Name: "TokenBucketRateLimitCF",
			Valid: &TokenBucketRateLimitCF{
				RefillRatePerSecond: 0, // zero means "use default value"
				BucketSize:          0, // zero means "use default value"
			},
		},
		{
			Name: "RedisCF",
			Valid: &RedisCF{
				RedisConfig: &RedisCF_Server{
					Server: &RedisServerCF{
						Address: "//path/to/socket.sock",
					},
				},
				PoolSize:  0,  // zero means "use default value"
				KeyPrefix: "", // empty means "use default value"
			},
		},
		{
			Name: "RedisCF",
			Valid: &RedisCF{
				RedisConfig: &RedisCF_Server{
					Server: &RedisServerCF{
						Address: "address:6380",
					},
				},
				PoolSize:  0,  // zero means "use default value"
				KeyPrefix: "", // empty means "use default value"
			},
		},
		{
			Name: "RedisCF",
			Valid: &RedisCF{
				RedisConfig: &RedisCF_Server{
					Server: &RedisServerCF{
						Address: "127.0.0.1:6380",
					},
				},
				PoolSize:  0,  // zero means "use default value"
				KeyPrefix: "", // empty means "use default value"
			},
		},
		{
			Name: "AgentConfigurationCF",
			Valid: &AgentConfigurationCF{
				MaxConfigurationFileSize: 0, // zero means "use default value"
			},
		},
		{
			Name: "ListenAgentCF",
			Valid: &ListenAgentCF{
				ConnectionsPerTokenPerMinute: 0, // zero means "use default value"
			},
		},
	}
	testhelpers.AssertValid(t, tests)
}

func TestValidation_Invalid(t *testing.T) {
	tests := []testhelpers.InvalidTestcase{
		{
			ErrString: "validation error:\n - info_cache_ttl: value must be greater than or equal to 0s [duration.gte]",
			Invalid: &AgentCF{
				InfoCacheTtl:         durationpb.New(-1),
				RedisConnInfoRefresh: durationpb.New(1),
				RedisConnInfoTtl:     durationpb.New(2),
			},
		},
		{
			ErrString: "validation error:\n - info_cache_error_ttl: value must be greater than 0s [duration.gt]",
			Invalid: &AgentCF{
				InfoCacheErrorTtl:    durationpb.New(0),
				RedisConnInfoRefresh: durationpb.New(1),
				RedisConnInfoTtl:     durationpb.New(2),
			},
		},
		{
			ErrString: "validation error:\n - info_cache_error_ttl: value must be greater than 0s [duration.gt]",
			Invalid: &AgentCF{
				InfoCacheErrorTtl:    durationpb.New(-1),
				RedisConnInfoRefresh: durationpb.New(1),
				RedisConnInfoTtl:     durationpb.New(2),
			},
		},
		{
			ErrString: "validation error:\n - redis_conn_info_refresh should be < redis_conn_info_ttl [redis_conn_info]",
			Invalid: &AgentCF{
				RedisConnInfoRefresh: durationpb.New(1),
				RedisConnInfoTtl:     durationpb.New(1),
			},
		},
		{
			ErrString: "validation error:\n - redis_conn_info_refresh should be < redis_conn_info_ttl [redis_conn_info]",
			Invalid: &AgentCF{
				RedisConnInfoRefresh: durationpb.New(2),
				RedisConnInfoTtl:     durationpb.New(1),
			},
		},
		{
			ErrString: "validation error:\n - poll_period: value must be greater than 0s [duration.gt]",
			Invalid: &AgentConfigurationCF{
				PollPeriod: durationpb.New(0),
			},
		},
		{
			ErrString: "validation error:\n - poll_period: value must be greater than 0s [duration.gt]",
			Invalid: &AgentConfigurationCF{
				PollPeriod: durationpb.New(-1),
			},
		},
		{
			ErrString: "validation error:\n - usage_reporting_period: value must be greater than or equal to 0s [duration.gte]",
			Invalid: &ObservabilityCF{
				UsageReportingPeriod: durationpb.New(-1),
			},
		},
		{
			ErrString: "validation error:\n - refill_rate_per_second: value must be greater than or equal to 0 [double.gte]",
			Invalid: &TokenBucketRateLimitCF{
				RefillRatePerSecond: -1,
			},
		},
		{
			ErrString: "validation error:\n - dial_timeout: value must be greater than 0s [duration.gt]",
			Invalid: &RedisCF{
				RedisConfig: &RedisCF_Server{
					Server: &RedisServerCF{
						Address: "//path/to/socket.sock",
					},
				},
				DialTimeout: durationpb.New(0),
			},
		},
		{
			ErrString: "validation error:\n - dial_timeout: value must be greater than 0s [duration.gt]",
			Invalid: &RedisCF{
				RedisConfig: &RedisCF_Server{
					Server: &RedisServerCF{
						Address: "//path/to/socket.sock",
					},
				},
				DialTimeout: durationpb.New(-1),
			},
		},
		{
			ErrString: "validation error:\n - read_timeout: value must be greater than 0s [duration.gt]",
			Invalid: &RedisCF{
				RedisConfig: &RedisCF_Server{
					Server: &RedisServerCF{
						Address: "//path/to/socket.sock",
					},
				},
				ReadTimeout: durationpb.New(0),
			},
		},
		{
			ErrString: "validation error:\n - read_timeout: value must be greater than 0s [duration.gt]",
			Invalid: &RedisCF{
				RedisConfig: &RedisCF_Server{
					Server: &RedisServerCF{
						Address: "//path/to/socket.sock",
					},
				},
				ReadTimeout: durationpb.New(-1),
			},
		},
		{
			ErrString: "validation error:\n - write_timeout: value must be greater than 0s [duration.gt]",
			Invalid: &RedisCF{
				RedisConfig: &RedisCF_Server{
					Server: &RedisServerCF{
						Address: "//path/to/socket.sock",
					},
				},
				WriteTimeout: durationpb.New(0),
			},
		},
		{
			ErrString: "validation error:\n - write_timeout: value must be greater than 0s [duration.gt]",
			Invalid: &RedisCF{
				RedisConfig: &RedisCF_Server{
					Server: &RedisServerCF{
						Address: "//path/to/socket.sock",
					},
				},
				WriteTimeout: durationpb.New(-1),
			},
		},
		{
			ErrString: "validation error:\n - idle_timeout: value must be greater than 0s [duration.gt]",
			Invalid: &RedisCF{
				RedisConfig: &RedisCF_Server{
					Server: &RedisServerCF{
						Address: "//path/to/socket.sock",
					},
				},
				IdleTimeout: durationpb.New(0),
			},
		},
		{
			ErrString: "validation error:\n - idle_timeout: value must be greater than 0s [duration.gt]",
			Invalid: &RedisCF{
				RedisConfig: &RedisCF_Server{
					Server: &RedisServerCF{
						Address: "//path/to/socket.sock",
					},
				},
				IdleTimeout: durationpb.New(-1),
			},
		},
		{
			ErrString: "validation error:\n - redis_config: exactly one field is required in oneof [required]",
			Invalid:   &RedisCF{},
		},
		{
			ErrString: "validation error:\n - server.address: value length must be at least 1 bytes [string.min_bytes]",
			Invalid: &RedisCF{
				RedisConfig: &RedisCF_Server{},
			},
		},
		{
			ErrString: "validation error:\n - sentinel.master_name: value length must be at least 1 bytes [string.min_bytes]\n - sentinel.addresses: value must contain at least 1 item(s) [repeated.min_items]",
			Invalid: &RedisCF{
				RedisConfig: &RedisCF_Sentinel{},
			},
		},
		{
			ErrString: "validation error:\n - address: value length must be at least 1 bytes [string.min_bytes]",
			Invalid:   &RedisServerCF{},
		},
		{
			ErrString: "validation error:\n - master_name: value length must be at least 1 bytes [string.min_bytes]",
			Invalid: &RedisSentinelCF{
				Addresses: []string{"1:2"},
			},
		},
		{
			ErrString: "validation error:\n - addresses: value must contain at least 1 item(s) [repeated.min_items]",
			Invalid: &RedisSentinelCF{
				MasterName: "bla",
			},
		},
		{
			ErrString: "validation error:\n - addresses[0]: value length must be at least 1 bytes [string.min_bytes]",
			Invalid: &RedisSentinelCF{
				MasterName: "bla",
				Addresses:  []string{""},
			},
		},
		{
			ErrString: "validation error:\n - max_connection_age: value must be greater than 0s [duration.gt]",
			Invalid: &ListenAgentCF{
				MaxConnectionAge: durationpb.New(0),
			},
		},
		{
			ErrString: "validation error:\n - max_connection_age: value must be greater than 0s [duration.gt]",
			Invalid: &ListenAgentCF{
				MaxConnectionAge: durationpb.New(-1),
			},
		},
		{
			ErrString: "validation error:\n - address: value length must be at least 1 bytes [string.min_bytes]\n - address: value is empty, which is not a valid URI [string.uri_empty]\n - authentication_secret_file: value length must be at least 1 bytes [string.min_bytes]",
			Invalid:   &GitLabCF{},
		},
		{
			ErrString: "validation error:\n - gitlab: value is required [required]\n - redis: value is required [required]\n - api: value is required [required]\n - private_api: value is required [required]",
			Invalid:   &ConfigurationFile{},
		},
		{
			ErrString: "validation error:\n - authentication_secret_file: value length must be at least 1 bytes [string.min_bytes]",
			Invalid:   &ListenApiCF{},
		},
		{
			ErrString: "validation error:\n - max_connection_age: value must be greater than 0s [duration.gt]",
			Invalid: &ListenApiCF{
				AuthenticationSecretFile: "bla",
				MaxConnectionAge:         durationpb.New(0),
			},
		},
		{
			ErrString: "validation error:\n - max_connection_age: value must be greater than 0s [duration.gt]",
			Invalid: &ListenApiCF{
				AuthenticationSecretFile: "bla",
				MaxConnectionAge:         durationpb.New(-1),
			},
		},
		{
			ErrString: "validation error:\n - listen: value is required [required]",
			Invalid:   &ApiCF{},
		},
		{
			ErrString: "validation error:\n - listen: value is required [required]",
			Invalid:   &PrivateApiCF{},
		},
	}
	testhelpers.AssertInvalid(t, tests)
}
